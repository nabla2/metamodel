package nabla2.metamodel.datatype

class OrdinalNumberTest extends GroovyTestCase {

  static boolean valid(String value) {
    new OrdinalNumber(value).isValid()
  }

  void testAcceptingNullOrEmpty() {
    assert  valid("")
    assert  valid(null)
    assert !valid(" ")
  }

  void testAcceptingOnlyUnsignedNonZeroDigits() {
    assert  valid("1234")
    assert  valid("1230")
    assert !valid("0123")
    assert !valid("0")
    assert !valid("-123")
    assert !valid("+123")
    assert !valid("2 3")
    assert !valid("hogehoge")
  }
}
