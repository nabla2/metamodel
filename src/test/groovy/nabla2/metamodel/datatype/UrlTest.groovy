package nabla2.metamodel.datatype

class UrlTest extends GroovyTestCase {

  static boolean valid(String value) {
    new Url(value).isValid()
  }

  void testAcceptingNullOrEmpty() {
    assert  valid("")
    assert  valid(null)
    assert !valid(" ")
  }

  void testAcceptingOnlyValidUrlString() {
    assert  valid('http://www.example.com/')
    assert  valid('https://www.example.com/')
    assert !valid('http//www.example.com/')
    assert  valid('modal:hoge/fuga/piyo')
    assert  valid(
    'http://www.example.com/hoge/fuga'
    )
    assert  valid(
    'http://www.example.com/hoge/fuga?p1=v1&p2=v2'
    )
    assert  valid(
    'http://www.example.com/hoge/fuga/?p1=v1&p2=v2#hash'
    )

    assert valid('/')
    assert valid('/hoge/fuga')
  }
 }
