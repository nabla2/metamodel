package nabla2.metamodel.datatype

class MultiLineTextTest extends GroovyTestCase {

  static boolean valid(String value) {
    new MultiLineText(value).isValid()
  }

  void testRetrievingItsValue() {
    assert new MultiLineText('hoge\nfuga').get() == 'hoge\nfuga'
  }

  void testThatEmptyStringsShouldBeAccepted() {
    assert valid("")
    assert valid(null)
  }

  void testThatTextIncludingLineBreakShouldBeAccepted() {
    assert  valid(' ')
    assert  valid('hogehoge')
    assert  valid('hoge\nfuga')
    assert  valid('\r')
  }
}
