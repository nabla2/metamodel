package nabla2.metamodel.datatype

import java.util.regex.Pattern

class ParameterizedTextTest extends GroovyTestCase {
  static boolean valid(String value) {
    new ParameterizedText(value).isValid()
  }

  void testAcceptingNullOrEmpty() {
    assert  valid("")
    assert  valid(null)
  }

  void testRejectingInconsistentParameterBracket() {
    assert  valid("【画面項目名】に【文字列】と入力する")
    assert  valid("【画面項目名】【文字列】")
    assert  valid("【画面項目名】")
    assert  valid("【画面項目名】に【】と入力する")
    assert  valid(" ")
    assert  valid("画面項目名")
    assert !valid("【画面項目名】に【文字列】と入力する】")
    assert !valid("画面項目名】に【文字列】と入力する】")
    assert !valid("【画面項目名に【文字列】と入力する】")
    assert !valid("【画面項目名に【文字列】と入力する】")
  }

  void testThatParameterizedTextShouldBeSameAsItsFormatDefinition() {
    ParameterizedText text = new ParameterizedText(
        "【検索欄】に【hogehoge】と入力する(待機時間【2000】msec)"
    )
    assert  text.sameAs(new SingleLineText("【対象フィールド】に【入力内容】と入力する(待機時間【Wait】msec)"))
    assert  text.sameAs(new MultiLineText("【画面項目名】に【文字列】と入力する(待機時間【Wait】msec)"))
    assert  text.sameAs(new ParameterizedText("【画面項目名】 に 【文字列】\nと入力する(待機時間【Wait】msec)"))
    assert  text.sameAs(new SingleLineText("【】に【文字列】と入力する(待機時間【Wait】msec)"))
    assert !text.sameAs(new SingleLineText("【画面項目名】に【文字列】と入力する!(待機時間【Wait】msec)"))
    assert !text.sameAs(new SingleLineText("【画面項目名】に【文字列】と入力 する(待機時間【Wait】msec)"))
    assert !text.sameAs(new SingleLineText(""))
  }

  void testThatParameterizedTextAcceptsOptionalParameter() {
    ParameterizedText format = new ParameterizedText(
      "【項目名】に【入力内容】と入力する【待機時間:waitTime】"
    )
    assert format.sameAs("【検索欄】に【hogehoge】と入力する【待機時間:2000msec】")
    assert format.sameAs("【検索欄】に【hogehoge】と入力する")
    assert format.sameAs("【検索欄】に【hogehoge】と入力する【待機時間:waitTime】【待機時間:waitTime】")
    assert format.sameAs(new ParameterizedText("【検索欄】に【hogehoge】と入力する"))
    assert format.params == ["項目名", "入力内容", "待機時間:waitTime"]
    assert format.capture("【検索欄】に【hogehoge】と入力する【待機時間:2000msec】") == ["検索欄", "hogehoge", "待機時間:2000msec"]
    assert format.capture("【検索欄】に【hogehoge】と入力する") == ["検索欄", "hogehoge", null]

    assert new SingleLineText("【検索欄】に【hogehoge】と入力する").sameAs(format)
    assert new SingleLineText("【検索欄】に【hogehoge】と入力する【待機時間:2000msec】").sameAs(format)

    assert format.sameAs(new SingleLineText("【検索欄】に【hogehoge】と入力する"))
    assert format.sameAs(new SingleLineText("【検索欄】に【hogehoge】と入力する【待機時間:2000msec】"))
  }

  void testThatOpationalKeyedParammetersCanBeRetrievedAsMap() {
    ParameterizedText format = new ParameterizedText(
      "【項目名】に【入力内容】と入力する【待機時間:waitTime】【最大試行回数:maxRetry】"
    )
    assert format.captureOptions("【検索欄】に【hogehoge】と入力する【待機時間:2000msec】【最大試行回数:10】") == [
      waitTime:'2000msec',
      maxRetry:'10',
    ]
    assert format.captureOptions("【検索欄】に【hogehoge】と入力する【最大試行回数:10】【待機時間:2000msec】") == [
      waitTime:'2000msec',
      maxRetry:'10',
    ]
    assert format.captureOptions("【検索欄】に【hogehoge】と入力する【最大試行回数:10】") == [
      maxRetry:'10',
    ]
  }

  void testThatMultilineTextCanBeUsedAsAParameter() {
    ParameterizedText format = new ParameterizedText("【項目名】に【入力内容】と入力する")
    assert format.sameAs("【検索欄】に【hoge\nhoge】と入力する")
    assert format.capture("【検索欄】に【hoge\nhoge】と入力する") == ['検索欄', 'hoge\nhoge']
  }

  void testThatLineBreaksAroundPlaceHolderAreIgnored() {
     ParameterizedText format = new ParameterizedText(
      "【項目名】\n に【入力内容】と入力する\n\n  【待機時間:waitTime】"
    )
    assert format.sameAs("【検索欄】に【hogehoge】と入力する【待機時間:2000msec】")
  }

  void testThatParameterNameSeparatorCanBeUsedAsNormalLiteralWhenUsingSpecialSyntax() {
    ParameterizedText format = new ParameterizedText(
      "【項目名】\n に【入力内容】と入力する\n\n  【待機時間:waitTime】"
    )
    String param =  "【検索欄】に【:{hoge:0, fuga:1}】と入力する【待機時間:20:00】"
    assert format.sameAs(param)
    assert format.captureWithName(param).inject([:]) {map, e ->
      map[e.key] = e.value
      map
    } == [
      '項目名':'検索欄',
      '入力内容':'{hoge:0, fuga:1}',
      '待機時間:waitTime':'待機時間:20:00'
    ]
    new ParameterizedText(param).with {
      assert it.sameAs(format)
    }
  }

  void testCaptureWithName() {
    ParameterizedText format = new ParameterizedText(
      "【項目名】に【入力内容】と入力する【待機時間:waitTime】"
    )
    ParameterizedText format2 = new ParameterizedText(
      "【項目名】に【入力内容】と入力する【待機時間:waitTime】【待機時間2:waitTime】"
    )
    assert format.sameAs(format)
    assert format.sameAs(format2)
    assert format2.sameAs(format)
    assert format
          .captureWithName("【検索欄】に【hogehoge】と入力する【待機時間:2000msec】")
          .collect{ entry -> "${entry.key}:${entry.value}" }
          .toString( ) == [
            "項目名:検索欄", "入力内容:hogehoge", "待機時間:waitTime:待機時間:2000msec",
          ].toString()

    format = new ParameterizedText("いそいで入力する")
    assert format.captureWithName("いそいで入力する")
          .collect{ entry -> "${entry.key}:${entry.value}" } == []
  }

  static Pattern formatPattern(String format) {
    new ParameterizedText(format)
       .value
       .orElseThrow {new IllegalArgumentException()}
  }

  void testFormatPattern() {
    formatPattern("【画面項目名】に【文字列】と入力する").with {
      assert  it.matcher("【画面項目名】に【文字列】と入力する").matches()
      assert  it.matcher("【検索欄】に【hogehoge】と入力する").matches()
      assert  it.matcher("【検索欄】に  【hogehoge】と入力する").matches()
      assert !it.matcher("【検索欄】が【hogehoge】と入力する").matches()
      assert !it.matcher("【検索欄】が【hogehoge】と入力する。").matches()
      assert !it.matcher("【検索欄】が【hog【ehoge】と入力する。").matches()
    }
  }

  static List<String> paramNames(String format) {
    new ParameterizedText(format).params
  }
  void testParamNames() {
    assert  paramNames("【画面項目名】に【文字列】と入力する") == ['画面項目名', '文字列']
    assert  paramNames("画面項目名") == []
  }
}
