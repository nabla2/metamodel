package nabla2.metamodel.datatype

class BooleanFlagTest extends GroovyTestCase {

  static boolean valid(String value) {
    new BooleanFlag(value).isValid()
  }

  static boolean truthy(String value) {
    new BooleanFlag(value).isTruthy()
  }

  static boolean falsy(String value) {
    new BooleanFlag(value).isFalsy()
  }

  void testAcceptingNullOrEmpty() {
    assert  valid("")
    assert  valid(null)
    assert !valid(" ")
  }

  void testTruthy() {
    assert  valid(null)
    assert !truthy(null)
    assert !falsy(null)
    assert  valid("y")
    assert  truthy("y")
    assert !falsy("y")
    assert  valid("Yes")
    assert  truthy("Yes")
    assert !falsy("Yes")
    assert  valid("○")
    assert  truthy("○")
    assert !falsy("○")
    assert  valid("ON")
    assert  truthy("ON")
    assert !falsy("ON")

    assert  valid("N")
    assert !truthy("N")
    assert  falsy("N")
  }
}
