package nabla2.metamodel.datatype

class UrlWithPlaceholderTest extends GroovyTestCase {

  static boolean valid(String value) {
    new UrlWithPlaceholder(value).isValid()
  }

  void testAcceptingNullOrEmpty() {
    assert  valid("")
    assert  valid(null)
    assert !valid(" ")
  }

  void testAcceptingOnlyValidUrlString() {
    assert  valid('http://www.example.com/')
    assert  valid('https://www.example.com/')
    assert !valid('http//www.example.com/')
    assert  valid('modal:hoge/fuga/piyo')
    assert  valid(
    'http://www.example.com/hoge/fuga'
    )
    assert  valid(
    'http://www.example.com/hoge/fuga?p1=v1&p2=v2'
    )
    assert  valid(
    'http://www.example.com/hoge/fuga/?p1=v1&p2=v2#hash'
    )

    assert valid('/')
    assert valid('/hoge/fuga')
  }

  void testRejectsInconsistentPlaceholder() {
    assert  valid('/{hoge}/fuga')
    assert  valid('/hoge/{fuga}')
    assert !valid('/{hoge/fuga')
    assert !valid('/{hog}e/fuga')
    assert !valid('/{ho{ge}/fuga')
  }

  static boolean sameAs(String a, String b) {
    new UrlWithPlaceholder(a).sameAs(b)
  }

  void testSameAs() {
    assert sameAs(
      '/hoge/ja/welcome.html',
      '/hoge/ja/welcome.html',
    )
    assert sameAs(
      '/hoge/{lang}/welcome.html',
      '/hoge/ja/welcome.html',
    )
    assert !sameAs(
      '/hoge/lang/welcome.html',
      '/hoge/ja/welcome.html',
    )
    assert !sameAs(
      '/hoge/{lang}/welcome.html',
      '/fuga/ja/welcome.html',
    )
  }

  void testGetCapture() {
    new UrlWithPlaceholder('/hoge/{lang}/welcome.html').with {
      assert it.capture('/hoge/ja/welcome.html') == ['ja']
      assert it.capture('/hoge/en/welcome.html') == ['en']
      assert it.capture('/hoge/welcome.html') == []
    }

    new UrlWithPlaceholder('/{hogefuga}/{lang}/welcome.html').with {
      assert it.capture('/hoge/ja/welcome.html') == ['hoge', 'ja']
      assert it.capture('/fuga/en/welcome.html') == ['fuga', 'en']
      assert it.capture('/hoge/welcome.html') == []
    }

    new UrlWithPlaceholder('/hoge/ja/welcome.html').with {
      assert it.capture('/hoge/ja/welcome.html') == ['/hoge/ja/welcome.html']
    }
  }

  void testEmbeddingValuesToPathParameter() {
    new UrlWithPlaceholder('/{target}/{lang}/welcome.html').with {
      assert it.embedParams{ key ->
        switch (key) {
        case 'target' : return 'hogehoge'
        case 'lang'   : return 'ja'
        default :throw new IllegalStateException()
        }
      }.get() == '/hogehoge/ja/welcome.html'
      assert it.embededParamNames == ['target', 'lang']
    }
  }
}
