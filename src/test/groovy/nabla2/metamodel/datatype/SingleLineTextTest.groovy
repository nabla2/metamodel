package nabla2.metamodel.datatype

class SingleLineTextTest extends GroovyTestCase {

  static boolean valid(String value) {
    new SingleLineText(value).isValid()
  }

  void testJudgingItValidWhenItIsNullOrEmpty() {
    assert valid("")
    assert valid(null)
  }

  void testJudgingTheValueInvalidWhenItIncludeAnyLineBreaks() {
    assert  valid(' ')
    assert  valid('hogehoge')
    assert !valid('hoge\nfuga')
    assert !valid('\r')
  }
}
