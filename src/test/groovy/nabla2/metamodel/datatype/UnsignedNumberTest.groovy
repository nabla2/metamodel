package nabla2.metamodel.datatype

class UnsignedNumberTest extends GroovyTestCase {

  static boolean valid(String value) {
    new UnsignedNumber(value).isValid()
  }

  void testAcceptingNullOrEmpty() {
    assert  valid("")
    assert  valid(null)
    assert !valid(" ")
  }

  void testAcceptingText() {
    assert  valid("1234")
    assert  valid("1230")
    assert  valid("0")
    assert  valid("12.34")
    assert  valid("0.1234")
    assert  valid("12.3400")
    assert !valid(".1234")
    assert !valid("0123")
    assert !valid("+123")
    assert !valid("2 3")
    assert !valid("hoge23hoge")
  }
}
