package nabla2.metamodel.template

import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table

class EntityValidatorGroovyTest extends GroovyTestCase {

  File model = new File('./model/metamodel.xlsx')

  Observable<Table> table$ = ExcelWorkbookLoader.from(model).table$

  void testGeneratorSource() {

    Entity entity = Entity.from(table$).test().values().find {
      it.className == 'Entity'
    }

    new EntityValidatorGroovy(entity).source.with {
      assert it.contains("""\
      |  Observable<ConstraintViolation<Entity>> validate(Observable<Table> table\$) {
      |    Observable<Entity> entity\$ = Entity.from(table\$)
      |    Observable.fromArray(
      |      validateUniqueConstraint(entity\$),
      |      validateCardinalityConstraint(entity\$),
      |      validatePropertyType(entity\$),
      |      validateRequiredProperty(entity\$),
      |    )
      |    .flatMap() { it }
      |  }
      """.stripMargin().trim())

      assert it.contains("""\
      |  static Observable<ConstraintViolation<Entity>> validateRequiredProperty(Observable<Entity> entity\$) {
      |    entity\$.map { entity ->
      |      Observable.fromIterable([
      |        entity.name.literal.map{''}.orElse('名称'),
      |        entity.identifier.literal.map{''}.orElse('識別子'),
      |        entity.namespace.literal.map{''}.orElse('名前空間'),
      |        entity.domainName.literal.map{''}.orElse('領域名'),
      |      ]
      |      .findAll{!it.empty}
      |      .collect { m -> new ConstraintViolation<Entity>(
      |        causedBy: [entity],
      |        message: MessageFormat.format(
      |          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
      |          m,
      |          entity
      |        )
      |      )})
      |    }
      |    .flatMap() {it}
      |  }
      """.stripMargin().trim())

      assert it.contains("""\
      |  static Observable<ConstraintViolation<Entity>> validatePropertyType(Observable<Entity> entity\$) {
      |    entity\$.map { entity ->
      |      Observable.fromIterable([
      |        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
      |        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
      |        entity.namespace.messageIfInvalid.map{['名前空間', it]}.orElse(null),
      |        entity.domainName.messageIfInvalid.map{['領域名', it]}.orElse(null),
      |      ]
      |      .findAll()
      |      .collect { m -> new ConstraintViolation<Entity>(
      |        causedBy: [entity],
      |        message: MessageFormat.format(
      |          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
      |          m[0],
      |          m[1],
      |          entity
      |        )
      |      )})
      |    }
      |    .flatMap() {it}
      |  }
      """.stripMargin().trim())
    }
  }

}
