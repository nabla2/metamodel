package nabla2.metamodel.template

import nabla2.metamodel.MetamodelEntityLoader
import nabla2.metamodel.model.Entity

class EntityClassGroovyTest extends GroovyTestCase {

  private loader = MetamodelEntityLoader.from(
    new File('./model/metamodel.xlsx')
  )

  void testGeneratingAccessorForRelationship() {
    loader.entity$.test().values().findAll{it.className == 'Entity'}.with {
      assert it.size() == 1
      Entity entity = it.first()
      EntityClassGroovy template = new EntityClassGroovy(entity)
      assert template.relPath == 'nabla2/metamodel/model/Entity.groovy'
      template.source.with {
        assert it.contains("""\
        |  /**
        |   * プロパティリスト
        |   */
        |  @Memoized
        |  @JsonIgnore
        |  List<Property> getProperties() {
        |    Property.from(_table\$).filter {
        |      this.name.sameAs(it.entityName)
        |    }.toList().blockingGet()
        |  }
        """.stripMargin().trim())
      }
    }
  }
}
