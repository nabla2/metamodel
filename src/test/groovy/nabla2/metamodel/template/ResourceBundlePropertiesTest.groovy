package nabla2.metamodel.template

import nabla2.metamodel.MetamodelEntityLoader

class ResourceBundlePropertiesTest extends GroovyTestCase {

  private loader = MetamodelEntityLoader.from(
    new File('./model/metamodel.xlsx')
  )

  void testGenerateMessageResourceBundle() {
    ResourceBundleProperties template = new ResourceBundleProperties(
      loader.localizedTextBundle$.test().values().find {
        it.name.sameAs('Metamodel関連メッセージ') &&
        it.locale.sameAs('en')
      }
    )

    assert template.bundle.isDefault.truthy

    assert template.relPath == 'metamodel_messages.properties'

    template.source.with {
      assert it.contains("""
      |duplicated_entities=Duplicated {0} between following {1}(s):\\n{2}\\nand\\n{3}
      """.trim().stripMargin())
    }
  }
}
