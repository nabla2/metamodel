package nabla2.metamodel.model

import nabla2.metamodel.MetamodelEntityLoader

class DomainTest extends GroovyTestCase {

  MetamodelEntityLoader loader = MetamodelEntityLoader.from(
    new File('./model/metamodel.xlsx')
  )

  void testValidation() {
    loader.constraintViolation$.test().with {
      assert it.values().empty
    }
  }

  void testRelationships() {
    loader.domain$.test().with {
      assert it.valueCount() == 1
      Domain metamodel = it.values().first()
      assert metamodel.name.sameAs('メタモデル')
      assert metamodel.entities.size() == 13
      assert metamodel.generatorTasks.size() == 4
    }
  }
}
