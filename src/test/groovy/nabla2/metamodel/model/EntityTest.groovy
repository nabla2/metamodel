package nabla2.metamodel.model

import nabla2.metamodel.MetamodelEntityLoader

class EntityTest extends GroovyTestCase {

  MetamodelEntityLoader loader = MetamodelEntityLoader.from(
    new File('./model/metamodel.xlsx')
  )

  void testValidator() {
    loader.constraintViolation$.test().with {
      assert it.values().empty
    }
  }

  void testLoadingEntities() {
    loader.entity$.test().with {
      assertNoErrors()
      Entity domain = values().find { it.name.sameAs('領域') }
      domain.with {
        assert className == 'Domain'
        assert relationships.size() == 2
        assert properties.size() == 3
        properties.findAll{it.name.sameAs('識別子')}.with {
          assert it.size() == 1
          Property identifier = it.first()
          assert identifier.constraint.sameAs('*')
          identifier.type.with {
            assert it.name.sameAs('Java識別子')
          }
        }
      }
    }
  }
}
