package nabla2.metamodel.model

import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.MetamodelEntityLoader
import nabla2.metamodel.model.constraint.ConstraintViolation

class EntityValidatorTest extends GroovyTestCase {

  @Memoized
  static Observable<ConstraintViolation<?>> violation$(String name) {
    MetamodelEntityLoader.from(
      Class.getResourceAsStream(
        "/EntityValidatorTest/${name}.xlsx"
      )
    )
    .constraintViolation$
  }

  void testReportingDupliacatiedDataInUniqueColumn() {
    violation$('unique_key_duplication').test().with {
      assert it.valueCount() == 1
      it.values().first().with {
        assert it instanceof ConstraintViolation<Entity>
        ConstraintViolation<Entity> v = it
        assert v.message.contains(
          'Duplicated 名称 between following エンティティ(s):'
        )
        assert v.causedBy.size() == 2
        v.causedBy.with {
          it.first().with {
            assert it.name.sameAs('エンティティ1')
            assert it._row.pageName == 'model'
            assert it._row.range == 'B8:E8'
          }
          it.last().with {
            assert it.name.sameAs('エンティティ1')
            assert it._row.pageName == 'model'
            assert it._row.range == 'B9:E9'
          }
        }
      }
    }
  }

  void testReportingAbsenceOfRequiredKey() {
    violation$('absence_of_required_key').test().values().with {
      assert it.size() == 5
      it.first().with {
        assert it instanceof ConstraintViolation<Entity>
        ConstraintViolation<Entity> v = it
        assert v.message.contains("""
        |The Following entity refers to another entity 領域 which has value(s):
        |名称=hoge
        |but there is no such a record.:
        """.trim().stripMargin()
        )
        assert v.causedBy.size() == 1
        v.causedBy.first().with {
          assert it.name.sameAs('エンティティ1')
          assert it._row.range == 'B8:E8'
        }
      }
    }
  }

  void testReportingAbsenceOfRequiredValue() {
    violation$('absence_of_required_value').test().values().with {
      assert it.size() == 1
      it.first().with {
        assert it instanceof ConstraintViolation<Entity>
        ConstraintViolation<Entity> v = it
        assert v.message.contains("""
        |The value of 識別子 of the following entity must not be null:
        """.trim().stripMargin()
        )
        assert v.causedBy.size() == 1
        v.causedBy.first().with {
          assert it.name.sameAs('エンティティ1')
          assert it._row.range == 'B8:F8'
        }
      }
    }
  }

  void testReportingInvalidDataTypeError() {
    violation$('invalid_data_type').test().values().with {
      assert it.size() == 2
      it.find{
        it.causedBy.first() instanceof Domain
      }.with {
        assert it != null
        assert it instanceof ConstraintViolation<Domain>
        assert it.causedBy.size() == 1
        it.causedBy.first().with {
          assert it instanceof Domain
          assert it.name.sameAs('領域1')
        }
        assert it.message.contains("""\
        |Only alphabet, digit and two symbols ('\$' and '_') are allowed.
        """.stripMargin().trim())
      }
    }
  }
}
