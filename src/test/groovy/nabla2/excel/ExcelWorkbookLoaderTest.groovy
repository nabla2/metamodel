package nabla2.excel

import nabla2.metamodel.MetamodelEntityLoader
import nabla2.metamodel.model.ExternalResource

class ExcelWorkbookLoaderTest extends GroovyTestCase {

  static MetamodelEntityLoader load(String resourceName) {
    MetamodelEntityLoader.from(
      Class.getResource(
        "/ExcelWorkbookLoaderTest/${resourceName}.xlsx"
      ).openStream()
    )
  }

  void testCommonPropertyNotation() {
    load('common_property_notation')
    .with { loader ->
      loader.constraintViolation$.test().values().with {
        assert it.size() == 0
      }
      loader.domain$.test().values().with {
        assert it.size() == 1
        assert it.first().name.sameAs('領域A')
      }
      loader.entity$.test().values().with {
        assert it.size() == 2
        assert it.every {
          entity -> entity.domain.name.sameAs('領域A')
        }
      }
    }
  }

  void testLineCommentNotation() {
    load('line_comment_notation')
    .with { loader ->
      loader.constraintViolation$.test().with {
        it.assertNoErrors()
        assert it.values().size() == 0
      }
      loader.domain$.test().values().with {
        assert it.size() == 1
        assert it.first().name.sameAs('領域A')
      }
      loader.entity$.test().with {
        it.assertNoErrors()
        it.values().with {
          assert it.size() == 2
          assert it[0].name.sameAs('エンティティ1')
          assert it[1].name.sameAs('エンティティ3')
        }
      }
    }
  }

  void testTreatmentOfBlankCells() {
    load('treatment_of_blank_cells')
    .with { loader ->
      loader.constraintViolation$.test().values().with {
        assert it.size() == 0
      }
      loader.externalResource$.test().values().with {
        assert it.size() == 3
        it.find {
          it.name.sameAs('Chrome Webドライバ(Windows64bit)')
        }.with {
          assert it.os.sameAs('win')
          assert it.architecture.sameAs('x64')
        }
        it.find {
          it.name.sameAs('Chrome Webドライバ(Mac64bit)')
        }.with {
          assert it.os.sameAs('mac')
          assert it.architecture.empty
        }
        it.find {
          it.name.sameAs('Jacoco Agent')
        }.with {
          assert it.os.empty
          assert it.architecture.empty
        }
      }
    }
  }
}
