package nabla2.generator.model

import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.generator.template.GeneratorGradlePluginGroovy
import nabla2.generator.template.GeneratorGradleTaskGroovy
import nabla2.generator.template.GradlePluginProperties
import nabla2.metamodel.model.Domain
import nabla2.metamodel.model.Table

class GeneratorTaskTest extends GroovyTestCase {

  File model = new File('./model/metamodel.xlsx')

  Observable<Table> getTable$() {
    ExcelWorkbookLoader.from(model).table$
  }

  /**
   * ExcelからGenerator定義をロードするテスト
   */
  void testLoadingEntities() {
    new GeneratorTaskValidator().validate(table$).test().with {
      assert values().size() == 0
    }
    GeneratorTask.from(table$).test().with {
      assert values().collect{it.name.toString()}.sort() == [
        'エンティティクラスを生成', 'リソースバンドルを生成', '自動生成Gradleタスクを生成', '外部リソースを取得'
      ].sort()
    }
  }

  /**
   * Gradleプラグイン定義クラスおよびマニフェストXMLを生成するテスト
   */
  void testGeneratingGradlePlugin() {
    Domain metamodel = Domain.from(table$).test().values().find {
      it.name.sameAs('メタモデル')
    }

    metamodel.with {
      assert metamodel.fqn == 'nabla2.Metamodel'
      assert metamodel.generatorTasks.collect{it.name.toString()}.sort() == [
        'エンティティクラスを生成', 'リソースバンドルを生成', '外部リソースを取得', '自動生成Gradleタスクを生成',
      ].sort()
    }

    new GeneratorGradlePluginGroovy(metamodel).with {
      assert it.source.contains("""\
      |  void apply(Project project) {
      |    register(
      |      project,
      |      GenerateEntityTask,
      |      GenerateEntityTask.Settings,
      |    )
      |    register(
      |      project,
      |      GenerateResourceBundleTask,
      |      GenerateResourceBundleTask.Settings,
      |    )
      |    register(
      |      project,
      |      GenerateGeneratorTask,
      |      GenerateGeneratorTask.Settings,
      |    )
      |    register(
      |      project,
      |      DownloadExternalResourceTask,
      |      DownloadExternalResourceTask.Settings,
      |    )
      |  }
      """.stripMargin().trim())
    }

    new GradlePluginProperties(metamodel).with {
      assert it.source.contains("""\
      |implementation-class=nabla2.gradle.MetamodelGradlePlugin
      """.stripMargin().trim())
    }

  }

  /**
   * Gradleタスク自体を自動生成するGradleタスクのテスト
   */
  void testGeneratingGeneratorGradleTask() {

    GeneratorTask generatorTask = GeneratorTask.from(table$).test().values().find {
      it.name.sameAs('自動生成Gradleタスクを生成')
    }

    generatorTask.with {
      assert it.fqn == 'nabla2.generator.gradle.GenerateGeneratorTask'
      assert it.templateList.collect{it.name.toString()}.sort() == [
        'Gradleプラグイン定義', '自動生成Gradleタスク', '自動生成Gradleプラグイン'
      ].sort()
    }

    new GeneratorGradleTaskGroovy(generatorTask).with {

      assert source.contains("""\
      |package nabla2.generator.gradle
      |
      |import io.reactivex.Observable
      |import nabla2.excel.ExcelWorkbookLoader
      |import nabla2.generator.model.GeneratorTask
      |import nabla2.generator.model.GeneratorTaskValidator
      |import nabla2.generator.model.GeneratorTemplateValidator
      |import nabla2.generator.template.GeneratorGradlePluginGroovy
      |import nabla2.generator.template.GeneratorGradleTaskGroovy
      |import nabla2.generator.template.GradlePluginProperties
      |import nabla2.gradle.TaskSettings
      |import nabla2.metamodel.model.DataTypeValidator
      |import nabla2.metamodel.model.Domain
      |import nabla2.metamodel.model.DomainValidator
      |import nabla2.metamodel.model.EntityTraitValidator
      |import nabla2.metamodel.model.EntityValidator
      |import nabla2.metamodel.model.ExternalResourceValidator
      |import nabla2.metamodel.model.LocalizedTextBundleValidator
      |import nabla2.metamodel.model.LocalizedTextValidator
      |import nabla2.metamodel.model.PropertyValidator
      |import nabla2.metamodel.model.RelationshipPropertyValidator
      |import nabla2.metamodel.model.RelationshipValidator
      |import nabla2.metamodel.model.Table
      |import nabla2.metamodel.model.TraitValidator
      |import org.gradle.api.DefaultTask
      |import org.gradle.api.file.FileCollection
      |import org.gradle.api.tasks.InputFiles
      |import org.gradle.api.tasks.Optional
      |import org.gradle.api.tasks.OutputDirectory
      |import org.gradle.api.tasks.TaskAction
      """.stripMargin().trim())

      assert it.source.contains("""\
      |  @TaskAction
      |  void exec() {
      |    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      |      ExcelWorkbookLoader.from(workbook).table\$
      |    }.with {
      |      validate(it)
      |      GeneratorTask.from(it)
      |      .filter {
      |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      |      }.subscribe {
      |        new GeneratorGradleTaskGroovy(it).with {
      |          it.regenerationMark = "@author nabla2.metamodel.generator"
      |          it.generateTo(new File(baseDir, 'src/main/groovy'))
      |        }
      |      }
      |      Domain.from(it)
      |      .filter {
      |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      |      }.subscribe {
      |        new GeneratorGradlePluginGroovy(it).with {
      |          it.regenerationMark = "@author nabla2.metamodel.generator"
      |          it.generateTo(new File(baseDir, 'src/main/groovy'))
      |        }
      |      }
      |      Domain.from(it)
      |      .filter {
      |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      |      }.subscribe {
      |        new GradlePluginProperties(it).with {
      |          it.regenerationMark = "@author nabla2.metamodel.generator"
      |          it.generateTo(new File(baseDir, 'src/main/resources'))
      |        }
      |      }
      |    }
      |  }
      """.stripMargin().trim())

      assert it.source.contains("""\
      |  void validate(Observable<Table> table\$) {
      |    Observable.fromArray(
      |      new DomainValidator().validate(table\$),
      |      new EntityValidator().validate(table\$),
      |      new ExternalResourceValidator().validate(table\$),
      |      new PropertyValidator().validate(table\$),
      |      new LocalizedTextBundleValidator().validate(table\$),
      |      new LocalizedTextValidator().validate(table\$),
      |      new DataTypeValidator().validate(table\$),
      |      new RelationshipValidator().validate(table\$),
      |      new RelationshipPropertyValidator().validate(table\$),
      |      new EntityTraitValidator().validate(table\$),
      |      new TraitValidator().validate(table\$),
      |      new GeneratorTemplateValidator().validate(table\$),
      |      new GeneratorTaskValidator().validate(table\$),
      |    )
      |    .flatMap {it}
      |    .toList()
      |    .blockingGet().with {
      |      if (!it.empty) throw new IllegalStateException(
      |        "Invalid records are found: \${it}"
      |      )
      |    }
      |  }
      """.stripMargin().trim())
    }
  }

  /**
   * エンティティクラス(Groovy)を生成するGradleタスクのテスト
   */
  void testGeneratingEntityGenerateGradleTask() {

    GeneratorTask entityClassTask = GeneratorTask.from(table$).test().values().find {
      it.name.sameAs('エンティティクラスを生成')
    }

    entityClassTask.with {
      assert it.fqn == 'nabla2.metamodel.gradle.GenerateEntityTask'
      assert it.templateList.collect{it.name.toString()}.sort() == [
        'エンティティクラス(Groovy)',
        'エンティティバリデータ',
        'エンティティローダ',
      ].sort()
    }

    new GeneratorGradleTaskGroovy(entityClassTask).with {

      assert source.contains("""\
      |  @TaskAction
      |  void exec() {
      |    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      |      ExcelWorkbookLoader.from(workbook).table\$
      |    }.with {
      |      validate(it)
      |      Entity.from(it)
      |      .filter {
      |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      |      }.subscribe {
      |        new EntityClassGroovy(it).with {
      |          it.regenerationMark = "@author nabla2.metamodel.generator"
      |          it.generateTo(new File(baseDir, 'src/main/groovy'))
      |        }
      |      }
      |      Entity.from(it)
      |      .filter {
      |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      |      }.subscribe {
      |        new EntityValidatorGroovy(it).with {
      |          it.regenerationMark = "@author nabla2.metamodel.generator"
      |          it.generateTo(new File(baseDir, 'src/main/groovy'))
      |        }
      |      }
      |      Domain.from(it)
      |      .filter {
      |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      |      }.subscribe {
      |        new EntityLoaderGroovy(it).with {
      |          it.regenerationMark = "@author nabla2.metamodel.generator"
      |          it.generateTo(new File(baseDir, 'src/main/groovy'))
      |        }
      |      }
      |    }
      |  }
      """.stripMargin().trim())
    }
  }
}

