package nabla2.generator.template

import groovy.transform.Memoized
import nabla2.excel.ExcelWorkbookLoader
import nabla2.metamodel.model.Domain
import nabla2.metamodel.template.EntityLoaderGroovy

class EntityLoaderGroovyTest extends GroovyTestCase {

  @Memoized
  static EntityLoaderGroovy getTemplate() {
    Domain metamodel = Domain
                      .from(ExcelWorkbookLoader.from(new File('./model/metamodel.xlsx')).table$)
                      .filter{it.name.sameAs('メタモデル')}
                      .blockingFirst()

    new EntityLoaderGroovy(metamodel)
  }

  void testGetRelPath() {
    assert template.relPath == 'nabla2/metamodel/MetamodelEntityLoader.groovy'
  }

  void testImportStatements() {
    assert template.source.contains("""\
    |import groovy.transform.Canonical
    |import io.reactivex.Observable
    |import nabla2.excel.ExcelWorkbookLoader
    |import nabla2.generator.model.GeneratorTask
    |import nabla2.generator.model.GeneratorTaskValidator
    |import nabla2.generator.model.GeneratorTemplate
    |import nabla2.generator.model.GeneratorTemplateValidator
    |import nabla2.metamodel.model.DataType
    |import nabla2.metamodel.model.DataTypeValidator
    |import nabla2.metamodel.model.Domain
    |import nabla2.metamodel.model.DomainValidator
    |import nabla2.metamodel.model.Entity
    |import nabla2.metamodel.model.EntityTrait
    |import nabla2.metamodel.model.EntityTraitValidator
    |import nabla2.metamodel.model.EntityValidator
    |import nabla2.metamodel.model.ExternalResource
    |import nabla2.metamodel.model.ExternalResourceValidator
    |import nabla2.metamodel.model.LocalizedText
    |import nabla2.metamodel.model.LocalizedTextBundle
    |import nabla2.metamodel.model.LocalizedTextBundleValidator
    |import nabla2.metamodel.model.LocalizedTextValidator
    |import nabla2.metamodel.model.Property
    |import nabla2.metamodel.model.PropertyValidator
    |import nabla2.metamodel.model.Relationship
    |import nabla2.metamodel.model.RelationshipProperty
    |import nabla2.metamodel.model.RelationshipPropertyValidator
    |import nabla2.metamodel.model.RelationshipValidator
    |import nabla2.metamodel.model.Table
    |import nabla2.metamodel.model.Trait
    |import nabla2.metamodel.model.TraitValidator
    |import nabla2.metamodel.model.constraint.ConstraintViolation
    """.stripMargin().trim())
  }

  void testClassDefinition() {
    assert template.source.contains("""\
    |  final Observable<Table> table\$
    |
    |  private MetamodelEntityLoader(File file) {
    |    table\$ = ExcelWorkbookLoader.from(file).table\$
    |  }
    |
    |  private MetamodelEntityLoader(InputStream stream) {
    |    table\$ = ExcelWorkbookLoader.from(stream).table\$
    |  }
    |
    |  static MetamodelEntityLoader from(File file) {
    |    new MetamodelEntityLoader(file)
    |  }
    |
    |  static MetamodelEntityLoader from(InputStream stream) {
    |    new MetamodelEntityLoader(stream)
    |  }
    """.stripMargin().trim())
  }

  void testPropertiesForStremOfEntities() {
    assert template.source.contains("""\
    |  Observable<Entity> getEntity\$() {
    |    Entity.from(table\$)
    |  }
    """.stripMargin().trim())
    assert template.source.contains("""\
    |  Observable<Domain> getDomain\$() {
    |    Domain.from(table\$)
    |  }
    """.stripMargin().trim())
   assert template.source.contains("""\
    |  Observable<ConstraintViolation<?>> getConstraintViolation\$() {
    |    Observable.fromArray(
    |      new DomainValidator().validate(table\$),
    |      new EntityValidator().validate(table\$),
    |      new ExternalResourceValidator().validate(table\$),
    |      new PropertyValidator().validate(table\$),
    |      new LocalizedTextBundleValidator().validate(table\$),
    |      new LocalizedTextValidator().validate(table\$),
    |      new DataTypeValidator().validate(table\$),
    |      new RelationshipValidator().validate(table\$),
    |      new RelationshipPropertyValidator().validate(table\$),
    |      new EntityTraitValidator().validate(table\$),
    |      new TraitValidator().validate(table\$),
    |      new GeneratorTemplateValidator().validate(table\$),
    |      new GeneratorTaskValidator().validate(table\$),
    |    ).flatMap{ it }
    |  }
    """.stripMargin().trim())
  }

}
