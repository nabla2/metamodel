package nabla2.generator.template

import nabla2.generator.model.GeneratorTask
import nabla2.metamodel.MetamodelEntityLoader

class GeneratorGradleTaskGroovyTest extends GroovyTestCase {

  void testGeneratingEntityGeneratorTask() {
    MetamodelEntityLoader
    .from(new File('model/metamodel.xlsx'))
    .generatorTask$
    .test().values().with {
      assert it.size() == 4
      GeneratorTask generateEntities = it.findAll{
        it.name.sameAs('エンティティクラスを生成')
      }.with {
        assert it.size() == 1
        it
      }.first()

      GeneratorGradleTaskGroovy template = new GeneratorGradleTaskGroovy(generateEntities)

      assert template.relPath == 'nabla2/metamodel/gradle/GenerateEntityTask.groovy'
      template.source.with {
        assert it.contains("""\
        |package nabla2.metamodel.gradle
        |
        |import io.reactivex.Observable
        |import nabla2.excel.ExcelWorkbookLoader
        |import nabla2.generator.model.GeneratorTaskValidator
        |import nabla2.generator.model.GeneratorTemplateValidator
        |import nabla2.gradle.TaskSettings
        |import nabla2.metamodel.model.DataTypeValidator
        |import nabla2.metamodel.model.Domain
        |import nabla2.metamodel.model.DomainValidator
        |import nabla2.metamodel.model.Entity
        |import nabla2.metamodel.model.EntityTraitValidator
        |import nabla2.metamodel.model.EntityValidator
        |import nabla2.metamodel.model.ExternalResourceValidator
        |import nabla2.metamodel.model.LocalizedTextBundleValidator
        |import nabla2.metamodel.model.LocalizedTextValidator
        |import nabla2.metamodel.model.PropertyValidator
        |import nabla2.metamodel.model.RelationshipPropertyValidator
        |import nabla2.metamodel.model.RelationshipValidator
        |import nabla2.metamodel.model.Table
        |import nabla2.metamodel.model.TraitValidator
        |import nabla2.metamodel.template.EntityClassGroovy
        |import nabla2.metamodel.template.EntityLoaderGroovy
        |import nabla2.metamodel.template.EntityValidatorGroovy
        |import org.gradle.api.DefaultTask
        |import org.gradle.api.file.FileCollection
        |import org.gradle.api.tasks.InputFiles
        |import org.gradle.api.tasks.Optional
        |import org.gradle.api.tasks.OutputDirectory
        |import org.gradle.api.tasks.TaskAction
        """.stripMargin().trim())

        assert it.contains("""\
        |class GenerateEntityTask extends DefaultTask {
        |
        |  @InputFiles FileCollection workbooks
        |  @InputFiles @Optional FileCollection referringTo
        |  @OutputDirectory File baseDir
        |  String group = 'laplacian generator'
        |  String description = 'エンティティクラスを生成'
        |
        |  @TaskAction
        |  void exec() {
        |    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
        |      ExcelWorkbookLoader.from(workbook).table\$
        |    }.with {
        |      validate(it)
        |      Entity.from(it)
        |      .filter {
        |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
        |      }.subscribe {
        |        new EntityClassGroovy(it).with {
        |          it.regenerationMark = "@author nabla2.metamodel.generator"
        |          it.generateTo(new File(baseDir, 'src/main/groovy'))
        |        }
        |      }
        |      Entity.from(it)
        |      .filter {
        |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
        |      }.subscribe {
        |        new EntityValidatorGroovy(it).with {
        |          it.regenerationMark = "@author nabla2.metamodel.generator"
        |          it.generateTo(new File(baseDir, 'src/main/groovy'))
        |        }
        |      }
        |      Domain.from(it)
        |      .filter {
        |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
        |      }.subscribe {
        |        new EntityLoaderGroovy(it).with {
        |          it.regenerationMark = "@author nabla2.metamodel.generator"
        |          it.generateTo(new File(baseDir, 'src/main/groovy'))
        |        }
        |      }
        |    }
        |  }
        """.stripMargin().trim())

        assert it.contains("""\
        |  void validate(Observable<Table> table\$) {
        |    Observable.fromArray(
        |      new DomainValidator().validate(table\$),
        |      new EntityValidator().validate(table\$),
        |      new ExternalResourceValidator().validate(table\$),
        |      new PropertyValidator().validate(table\$),
        |      new LocalizedTextBundleValidator().validate(table\$),
        |      new LocalizedTextValidator().validate(table\$),
        |      new DataTypeValidator().validate(table\$),
        |      new RelationshipValidator().validate(table\$),
        |      new RelationshipPropertyValidator().validate(table\$),
        |      new EntityTraitValidator().validate(table\$),
        |      new TraitValidator().validate(table\$),
        |      new GeneratorTemplateValidator().validate(table\$),
        |      new GeneratorTaskValidator().validate(table\$),
        |    )
        |    .flatMap {it}
        |    .toList()
        |    .blockingGet().with {
        |      if (!it.empty) throw new IllegalStateException(
        |        "Invalid records are found: \${it}"
        |      )
        |    }
        |  }
        """.stripMargin().trim())

        assert it.contains("""\
        |  static class Settings extends TaskSettings<GenerateEntityTask> {
        |    FileCollection workbooks
        |    FileCollection referringTo
        |    File baseDir
        |    void setWorkbook(File workbook) {
        |      workbooks = project.files(workbook)
        |    }
        |    @Override
        |    void setup(GenerateEntityTask task) {
        |      task.workbooks = workbooks
        |      task.referringTo = referringTo ?: project.files([])
        |      task.baseDir = baseDir ?: new File('./')
        |    }
        |  }
        """.stripMargin().trim())
      }
    }
  }
}
