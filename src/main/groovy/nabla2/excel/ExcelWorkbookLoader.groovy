package nabla2.excel

import io.reactivex.Observable
import io.reactivex.subjects.ReplaySubject
import io.reactivex.subjects.Subject
import nabla2.metamodel.model.Cell
import nabla2.metamodel.model.Table
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory

class ExcelWorkbookLoader {

  final Subject<Cell> cell$ = ReplaySubject.create()

  static ExcelWorkbookLoader from(File workbook)  {
    new ExcelWorkbookLoader().load(
      workbook.toURI(),
      WorkbookFactory.create(workbook, null, true)
    )
  }

  static ExcelWorkbookLoader from(InputStream workbook)  {
    new ExcelWorkbookLoader().load(
      URI.create(""),
      WorkbookFactory.create(workbook)
    )
  }

  Observable<Table> getTable$() {
    Table.from(cell$)
  }

  private ExcelWorkbookLoader load(URI path, Workbook workbook) {
    workbook.withCloseable {
      workbook
      .findAll{ !it.sheetName.startsWith('##') }
      .each { sheet ->
        sheet.each { row ->
          long lastOfRow = row.findAll{ it.cellTypeEnum != CellType.BLANK }
                              .max{ it.columnIndex }
                              .with { it ? it.columnIndex : 0 }
          row.each { cell ->
            if (cell.cellTypeEnum == CellType.BLANK) return
            if (cell.cellTypeEnum != CellType.STRING) throw new IllegalStateException(
              "${sheet.sheetName}(row=${cell.rowIndex}:col=${cell.columnIndex}) Only STRING cell type is allowed but ${cell.cellTypeEnum}"
            )
            if (!cell.stringCellValue) throw new IllegalStateException(
              "Invalid cell at (${cell.rowIndex}:${cell.columnIndex}) '${new String(cell.stringCellValue)}' : ${cell.cellTypeEnum}"
            )
            new Cell(
              content     :cell.stringCellValue,
              rowIndex    :cell.rowIndex,
              colIndex    :cell.columnIndex,
              pageName    :cell.sheet.sheetName,
              book        :path,
              lastOfRow   :cell.columnIndex == lastOfRow,
              lastOfTable :isBlank(sheet.getRow(row.rowNum + 1)),
            ).with {
              if (!it.content.empty && it.content.trim().empty) throw new IllegalStateException(
                "Invalid cell: it contains whitespace characters only. $it"
              )
              cell$.onNext(it)
            }
          }
        }
      }
      cell$.onComplete()
    }
    this
  }

  static isBlank(Row row) {
    !row ||
    (row.lastCellNum == (short) -1) ||
    row.every{ it.cellTypeEnum == CellType.BLANK }
  }
}
