package nabla2.excel.addin.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.Domain

@Canonical
class IndexHtml implements TextFileTemplate {

  Domain domain

  @Override
  String getSource() {
  """\
  |<!DOCTYPE html>
  |<!-- ${regenerationMark} -->
  |<html>
  |<head>
  |  <meta charset="utf-8" />
  |  <title>${domain.name} DevTool</title>
  |  <script src="./node_modules/@microsoft/office-js/dist/office.js"></script>
  |  <style>
  |  .errors {
  |    font-size:smaller;
  |  }
  |  table {
  |    margin: 2px;
  |    width: 98%;
  |  }
  |  table, td, th {
  |    border: 1px solid #ccc;
  |    border-collapse: collapse;
  |    cursor: pointer;
  |  }
  |  th {
  |    background-color: #444;
  |    color: #eee;
  |  }
  |  table table tr:nth-child(2n+0) {
  |    background-color: #f6f6ff;
  |  }
  |  table table th {
  |    background-color: #ccc;
  |    color: #fff;
  |  }
  |  .detailMessage p:first-child {
  |    color: red;
  |  }
  |  .detailMessage table {
  |    font-size:x-small;
  |  }
  |  #message {
  |    color: blue;
  |  }
  |  </style>
  |</head>
  |<body>
  |  <div id="message"></div>
  |  <div id="main">Loading...</div>
  |  <script src="./dist/bundle.js"></script>
  |</body>
  |</html>
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'index.html'
  }
}
