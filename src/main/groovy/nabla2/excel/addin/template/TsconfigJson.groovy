package nabla2.excel.addin.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.Domain

@Canonical
class TsconfigJson implements TextFileTemplate {

  Domain domain

  @Override
  String getSource() {
    """\
    |{
    |  "//": "${regenerationMark}",
    |  "compilerOptions": {
    |    "outDir": "./dist/",
    |    "sourceMap": true,
    |    "noImplicitAny": true,
    |    "module": "commonjs",
    |    "target": "es2015",
    |    "jsx": "react"
    |  },
    |  "include": [
    |    "./src/**/*"
    |  ]
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "tsconfig.json"
  }
}
