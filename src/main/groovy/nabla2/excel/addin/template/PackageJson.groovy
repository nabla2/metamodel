package nabla2.excel.addin.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.Domain

@Canonical
class PackageJson implements TextFileTemplate {

  Domain domain

  @Override
  String getSource() {
    """\
    |{
    |  "//": "${regenerationMark}",
    |  "scripts": {
    |    "build": "webpack --display-error-details",
    |    "start": "webpack-dev-server"
    |  },
    |  "dependencies": {
    |    "@microsoft/office-js": "^1.1.2",
    |    "react": "^16.0.0",
    |    "react-dom": "^16.0.0",
    |    "marked": "^0.3.6"
    |  },
    |  "devDependencies": {
    |    "@types/office-js": "^0.0.49",
    |    "@types/react": "^16.0.17",
    |    "@types/react-dom": "^16.0.2",
    |    "@types/marked": "^0.3.0",
    |    "typescript": "^2.5.3",
    |    "webpack": "^3.8.1",
    |    "webpack-dev-server": "^2.9.3",
    |    "ts-loader": "^3.0.5",
    |    "raw-loader": "^0.5.1"
    |  }
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "package.json"
  }
}
