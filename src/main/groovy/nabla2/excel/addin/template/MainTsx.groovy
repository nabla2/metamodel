package nabla2.excel.addin.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.Domain

@Canonical
class MainTsx implements TextFileTemplate {

  Domain domain

  @Override
  String getSource() {
  """\
  |import * as React from 'react'
  |import * as ReactDOM from 'react-dom'
  |import * as marked from 'marked'
  |const doc = require('!!raw-loader!../errors.md')
  |
  |/**
  | * メインモジュール
  | * @author nabla2.metamodel.generator
  | */
  |class Main extends React.Component<{errors:ErrorRecord[]}, HostInfo> {
  |
  |  constructor(props:any) {
  |    super(props)
  |  }
  |
  |  focusError = (error:ErrorRecord) => (e:any) => {
  |    runInExcel((context, hostInfo) => {
  |      const range = context.workbook.worksheets.getItem(error.sheet).getRange(error.range);
  |      range.select();
  |    })
  |  }
  |
  |  render():JSX.Element {
  |    const {errors} = this.props
  |    if (!errors.length) return <h3 style={{color:'green'}}>OK</h3>
  |    return <div>
  |      <h3 style={{color:'red'}}>Found {errors.length} errors: (click error to jump)</h3>
  |      {errors.map(error => <table onClick={this.focusError(error)}>
  |      <thead>
  |      <tr>
  |        <th>book</th>
  |        <th>sheet</th>
  |        <th>range</th>
  |      </tr>
  |      </thead>
  |      <tbody>
  |      <tr>
  |        <td className="book">{error.book}</td>
  |        <td className="sheet">{error.sheet}</td>
  |        <td className="range">{error.range}</td>
  |      </tr>
  |      <tr>
  |        <td className="detailMessage"
  |            colSpan={3}
  |            dangerouslySetInnerHTML={{__html: marked(error.message)}}>
  |        </td>
  |      </tr>
  |      </tbody>
  |      </table>
  |      )}
  |    </div>
  |  }
  |}
  |
  |interface ErrorRecord {
  |  book:string
  |  sheet:string
  |  range:string
  |  message:string
  |}
  |
  |interface HostInfo {
  |  sheetName:string
  |}
  |
  |const readErrorsFrom = (doc:string):ErrorRecord[] => {
  |  const messages = /###([^#]+|##[^#])+/gm
  |  const location = /\\[([^\\]]+)\\]\\([^)]+\\)\\s*([^!]+)!([0-9A-Z:]+)/
  |  const errors = []
  |  let found
  |  while (found = messages.exec(doc)) {
  |    const message = found[1]
  |    const loc = location.exec(message)
  |    errors.push({
  |      book:  loc[1],
  |      sheet: loc[2],
  |      range: loc[3],
  |      message: message,
  |    })
  |  }
  |  return errors
  |}
  |
  |const doOnEachSheetChanges = (fn:(context:Excel.RequestContext, info:HostInfo)=>void):void => {
  |  Excel.run((context) => {
  |    context.workbook.onSelectionChanged.add(() => runInExcel(fn))
  |    return context.sync()
  |  })
  |}
  |
  |const runInExcel = (fn:(context:Excel.RequestContext, hostInfo:HostInfo) => void):OfficeExtension.IPromise<any> => {
  |  return Excel.run((context) => {
  |    const sheet = context.workbook.worksheets.getActiveWorksheet()
  |    sheet.load('name')
  |    return context.sync().then(() => ({sheetName: sheet.name}))
  |  })
  |  .then((hostInfo:HostInfo) => {
  |    return Excel.run((context) => {
  |      fn(context, hostInfo)
  |      return context.sync()
  |    }).catch(error => {
  |      writeLog(JSON.stringify(error.debugInfo || error))
  |    })
  |  })
  |}
  |
  |const writeLog = (message:string) => {
  |  const messageArea = document.getElementById('message')
  |  messageArea.textContent = messageArea.textContent + '\\n' + message
  |}
  |
  |const render = () => {
  |  const errors = doc ? readErrorsFrom(doc) : []
  |  ReactDOM.render(<Main errors={errors} />, document.getElementById('main'))
  |}
  |
  |Office.initialize = render
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "main.tsx"
  }
}
