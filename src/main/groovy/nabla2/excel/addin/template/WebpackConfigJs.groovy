package nabla2.excel.addin.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.Domain

@Canonical
class WebpackConfigJs implements TextFileTemplate {

  Domain domain

  @Override
  String getSource() {
    """\
    |// "${regenerationMark}",
    |const path = require('path')
    |
    |const config = {
    |  entry: ['./src/main.tsx'],
    |  output: {
    |    path: path.resolve(__dirname, 'dist'),
    |    publicPath: '/dist/',
    |    filename: 'bundle.js',
    |  },
    |  devtool: 'source-map',
    |  resolve: {
    |    extensions: ['.ts', '.tsx', '.js', '.json']
    |  },
    |  module: {
    |    rules: [{
    |      test: /\\.tsx?\$/,
    |      use : [ {loader: 'ts-loader'}],
    |      exclude: /node_modules/,
    |    }, {
    |      test: /\\.md\$/,
    |      use: [
    |        { loader: "raw-loader" },
    |      ]
    |    },
    |  ]},
    |  devServer: {
    |    port: 7777
    |  }
    |}
    |module.exports = config
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "webpack.config.js"
  }
}
