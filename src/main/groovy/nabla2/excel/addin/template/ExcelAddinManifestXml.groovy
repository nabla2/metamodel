package nabla2.excel.addin.template

import groovy.json.StringEscapeUtils
import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.Domain
import nabla2.metamodel.model.LocalizedText
import nabla2.metamodel.model.LocalizedTextBundle

@Canonical
class ExcelAddinManifestXml implements TextFileTemplate {

  Domain domain

  static String getAddinUrl() {
    "http://localhost:7777/"
  }

  static String getAddinId() {
    UUID.randomUUID()
  }

  String getDescription() {
    "${domain.name}開発ツール"
  }

  @Override
  String getSource() {
  """
  |<?xml version="1.0" encoding="UTF-8"?>
  |<!--
  |  ${domain.name}開発用Excelアドイン定義
  |  ${regenerationMark}
  |-->
  |<OfficeApp xmlns="http://schemas.microsoft.com/office/appforoffice/1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="TaskPaneApp">
  |  <Id>${addinId}</Id>
  |  <Version>1.0</Version>
  |  <ProviderName>Laplacian</ProviderName>
  |  <DefaultLocale>en-US</DefaultLocale>
  |  <DisplayName DefaultValue="${description}" />
  |  <Description DefaultValue="${description}"/>
  |  <AppDomains>
  |    <AppDomain>${addinUrl}</AppDomain>
  |  </AppDomains>
  |  <DefaultSettings>
  |    <SourceLocation DefaultValue="${addinUrl}" />
  |  </DefaultSettings>
  |  <Permissions>ReadWriteDocument</Permissions>
  |</OfficeApp>
  """.trim().stripMargin()
  }

  @Override
  String getRelPath() {
    "${domain.identifier.dasherized.get()}-manifest.xml"
  }
}
