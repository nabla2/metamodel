package nabla2.gradle

import org.gradle.api.Project
import nabla2.gradle.BasePlugin
import nabla2.metamodel.gradle.GenerateEntityTask
import nabla2.metamodel.gradle.GenerateResourceBundleTask
import nabla2.metamodel.gradle.InstallExcelAddin
import nabla2.generator.gradle.GenerateGeneratorTask
import nabla2.generator.gradle.DownloadExternalResourceTask

/**
 * メタモデル自動生成Gradleプラグイン
 *
 * @author nabla2.metamodel.generator
 */
class MetamodelGradlePlugin extends BasePlugin {

  void apply(Project project) {
    register(
      project,
      GenerateEntityTask,
      GenerateEntityTask.Settings,
    )
    register(
      project,
      GenerateResourceBundleTask,
      GenerateResourceBundleTask.Settings,
    )
    register(
      project,
      InstallExcelAddin,
      InstallExcelAddin.Settings,
    )
    register(
      project,
      GenerateGeneratorTask,
      GenerateGeneratorTask.Settings,
    )
    register(
      project,
      DownloadExternalResourceTask,
      DownloadExternalResourceTask.Settings,
    )
  }
}