package nabla2.gradle

import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import org.gradle.api.Project
import org.gradle.api.Task

@Builder(builderStrategy = SimpleStrategy)
abstract class TaskSettings<T extends Task> {
  Project project
  abstract void setup(T task)
}
