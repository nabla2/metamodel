package nabla2.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

abstract class BasePlugin implements Plugin<Project> {
  public <T extends Task> void register(Project project,
                                        Class<T> taskClass,
                                        Class<? extends TaskSettings<T>> settingsClass) {
    String taskName = (taskClass.simpleName -~ /Task$/).with {
      it[0].toLowerCase() + it.substring(1)
    }
    project.extensions.create(taskName, settingsClass)
    project.extensions.findByType(settingsClass).project = project
    project.afterEvaluate {
      project.task(taskName, type :taskClass) { task ->
        TaskSettings<T> settings = project.extensions.findByType(settingsClass)
        settings.setup(task)
      }
    }
  }
}
