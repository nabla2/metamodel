package nabla2.metamodel.trait

import groovy.json.StringEscapeUtils
import groovy.transform.Memoized
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.DataType
import nabla2.metamodel.model.Property

trait PropertyTrait<T extends Property> {

  abstract StrictJavaIdentifier getIdentifier()
  abstract DataType getType()
  abstract SingleLineText getName()
  abstract MultiLineText getDefaultValue()

  String getPropertyName() {
    identifier.lowerCamelized.get()
  }

  @Memoized
  Optional<String> getDefaultPropValue() {
    defaultValue.value.map { v ->
      '"' + StringEscapeUtils.escapeJava(v.replaceAll(/^"|"$/, '')) + '"'
    }
  }

  String assignmentJavaSnippet() {
    "${propertyName} : new ${type.className}(row['${name}']${defaultPropValue.map{v -> " ?: ${v}".toString()  }.orElse('')})"
  }

}
