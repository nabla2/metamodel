package nabla2.metamodel.trait

import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.model.LocalizedText

trait LocalizedTextTrait<T extends LocalizedText> {

  abstract JavaNamespace getKey()

  /**
   * Java定数名を返す
   * @return Java定数名
   */
  String getConstantName() {
    key.value.map{it.replace('.', '_').toUpperCase()}.orElse('')
  }
}
