package nabla2.metamodel.trait

import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.LocalizedTextBundle

trait LocalizedTextBundleTrait<T extends LocalizedTextBundle> {
  abstract StrictJavaIdentifier getIdentifier()

  String getResourceBundleId() {
    identifier.get()
  }
}
