package nabla2.metamodel.trait

import groovy.transform.stc.ClosureParams
import groovy.transform.stc.FirstParam
import groovy.transform.stc.FromString

trait Hierarchical<T extends Hierarchical> {

  abstract T getParent()

  abstract List<T> getChildren()

  /**
   * 最上位項目からこの項目に至るまでの各項目からなる配列を返す。
   * 先頭が最上位階層の項目、末尾がこの項目となる。
   * @return この項目に至る階層の配列
   */
  List<T> getHierarchy() {
    List<T> ancestors = [(T)this]
    while (ancestors.first().parent) {
      ancestors.add(0, (T)ancestors.first().parent)
    }
    ancestors
  }

  /**
   * 指定された項目からこの項目に至るまでの相対パスを返す。
   * @param from 基準項目
   * @return 相対パス
   */
  String relativePathFrom(T from, @ClosureParams(FirstParam.class) Closure<String> editComponents = null)  {
    if (!from) return path
    int found = hierarchy.findIndexOf{it == from}
    (found < 0) ? path \
                : toPath(hierarchy[found+1..-1], editComponents)
  }

  /**
   * このデータ項目の階層構造内の位置を表す文字列を返す。
   * 階層の最上位から項目の識別子を'.'で連結した文字列となる。
   * @return 階層構造内の位置を表す文字列
   */
  String getPath() {
    toPath(hierarchy)
  }

  static String toPath(List<T> items, Closure<String> editComponents = null) {
    items.collect {
      editComponents ? editComponents(it) : it.identifier.lowerCamelized.get()
    }.join('.')
  }

  /**
   * 指定された項目がこの項目の親もしくはそれより上の階層に存在するかどうかを返す。
   * @param mayBeAncestor 対象項目
   * @return 指定された項目がこの項目の親もしくはそれより上の階層に存在すればtrue
   */
  boolean isDescendentFrom(T mayBeAncestor) {
    if (mayBeAncestor == this) return false
    hierarchy.contains(mayBeAncestor)
  }

  /**
   * 親項目が存在しないかどうかを返す。
   * @return 親項目が存在しなければtrue
   */
  boolean isTopLevel() {
    this.parent == null
  }
}