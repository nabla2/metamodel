package nabla2.metamodel.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.gradle.GenerateEntityTaskOptionSet
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Property
import nabla2.metamodel.model.Relationship
import nabla2.metamodel.model.RelationshipProperty
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

@Canonical
class EntityValidatorGroovy implements TextFileTemplate {

  Entity entity

  @Memoized
  String getSource() {

  """\
  |package ${entity.namespace}
  |
  ${dependentModules.collect{"""\
  |import ${it}
  """.trim()}.join('\n')}
  |
  |/**
  | * ${entity.name}バリデータ
  | *
  | * ${regenerationMark}
  | */
  |class ${className}Validator implements ${Validator.simpleName}<${className}> {
  |  Observable<ConstraintViolation<${className}>> validate(Observable<Table> table\$) {
  |    Observable<${className}> entity\$ = ${className}.from(table\$)
  |    Observable.fromArray(
  |      validateUniqueConstraint(entity\$),
  |      validateCardinalityConstraint(entity\$),
  |      validatePropertyType(entity\$),
  |      validateRequiredProperty(entity\$),
  |    )
  |    .flatMap() { it }
  |  }
  |
  ${validateUniqueConstraintsSource}
  |
  ${validateCardinalityConstraintsSource}
  |
  ${validatePropertyTypeSource}
  |
  ${validateRequiredPropertySource}
  |}
  """.stripMargin().trim()
  }

  static List<String> getDependentModules() {
    [ 'java.text.MessageFormat',
      'groovy.json.JsonOutput',
      'io.reactivex.Observable',
      ConstraintViolation.canonicalName,
      Validator.canonicalName,
      Entity.canonicalName,
      Table.canonicalName,
    ]
    .sort().unique()
  }

  List<Relationship> getRequiredRelationships() {
    entity.relationships.findAll {
      it.cardinality.value.orElse('').with {
        it.contains('.1') || it.startsWith('1')
      }
    }
  }

  String getValidateCardinalityConstraintsSource() {
    """\
    |  static Observable<ConstraintViolation<$className>> validateCardinalityConstraint(Observable<$className> entity\$) {
    |    entity\$.map { entity ->
    |      List<ConstraintViolation> violations = []
    ${requiredRelationships.collect { rel ->
      boolean permitsNullReference = rel.cardinality.value.map{it == 'N' || it.contains('0.')}.orElse(false)
      List<RelationshipProperty> propsWithNullValue = rel.referenceProperties.findAll{ !it.nullValue.empty }
      String nullReferenceCheck = !permitsNullReference ? "" : propsWithNullValue.collect{ """\
      !entity.${it.property.identifier.lowerCamelized.get()}.sameAs('${it.nullValue}') &&
      """.trim()}.join(' ')
      if (permitsNullReference && !nullReferenceCheck) return ''
    """\
    |      if (${nullReferenceCheck}entity.${rel.identifier.lowerCamelized.get()} == null) {
    |        violations.add(new ConstraintViolation<${entity.identifier.upperCamelized.get()}>(
    |          causedBy: [entity],
    |          message : MessageFormat.format(
    |            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
    |            '${rel.referenceEntity.name}',
    |            "${rel.referenceProperties.collect{it.referencePropertyName + '=' + '${entity.' + it.property.identifier.lowerCamelized.get() + '}'}.join(', ')}",
    |            entity,
    |          )
    |        ))
    |      }
    """.trim()}.join('\n')}
    |      Observable.fromIterable(violations)
    |    }
    |    .flatMap() {it}
    |  }
    """.trim().replaceAll(/\n\s*\n/, '\n')
  }

  List<Property> getPk() {
    entity
    .properties
    .findAll{ it.constraint.value.orElse('').contains('#') }
  }

  String getValidateUniqueConstraintsSource() {
  """\
  |  static Observable<ConstraintViolation<${className}>> validateUniqueConstraint(Observable<${className}> entity\$) {
  |    entity\$.scan([:]) { entityOf, entity ->
  |      String key = JsonOutput.toJson([
  ${pk.collect {"""\
  |        ${it.identifier.lowerCamelized.get()} : entity.${it.identifier.lowerCamelized.get()}.value.map{it.toString()}.orElse(null),
  """}.join('').trim()}
  |      ])
  |      ${className} another = entityOf[key]
  |      entityOf[key] = entity
  |      entityOf['__duplicated__'] = another ? [another, entity] : null
  |      entityOf
  |    }.filter {
  |      it.get('__duplicated__') != null
  |    }.map {
  |      List<${className}> duplicated = it.get('__duplicated__')
  |      new ConstraintViolation<${className}>(
  |        causedBy : duplicated,
  |        message : MessageFormat.format(
  |          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
  |          '${pk.collect{it.name}.join(', ')}',
  |          '${entity.name}',
  |          duplicated[0],
  |          duplicated[1]
  |        )
  |      )
  |    }
  |  }
  """.trim()
  }

  String getValidatePropertyTypeSource() {
  """\
  |  static Observable<ConstraintViolation<${className}>> validatePropertyType(Observable<${className}> entity\$) {
  |    entity\$.map { entity ->
  |      Observable.fromIterable([
  ${entity.properties.collect { property -> """\
  |        entity.${property.identifier.lowerCamelized.get()}.messageIfInvalid.map{['${property.name}', it]}.orElse(null),
  """.trim()}.join('\n')}
  |      ]
  |      .findAll()
  |      .collect { m -> new ConstraintViolation<${className}>(
  |        causedBy: [entity],
  |        message: MessageFormat.format(
  |          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
  |          m[0],
  |          m[1],
  |          entity
  |        )
  |      )})
  |    }
  |    .flatMap() {it}
  |  }
  """.stripMargin().trim()
  }

  String getValidateRequiredPropertySource() {
  """\
  |  static Observable<ConstraintViolation<${className}>> validateRequiredProperty(Observable<${className}> entity\$) {
  |    entity\$.map { entity ->
  |      Observable.fromIterable([
  ${entity.properties.findAll{it.constraint.get().with {it.contains('#') && !it.contains('o') || it.contains('*')}}.collect { property -> """\
  |        entity.${property.identifier.lowerCamelized.get()}.literal.map{''}.orElse('${property.name}'),
  """.trim()}.join('\n')}
  |      ]
  |      .findAll{!it.empty}
  |      .collect { m -> new ConstraintViolation<${className}>(
  |        causedBy: [entity],
  |        message: MessageFormat.format(
  |          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
  |          m,
  |          entity
  |        )
  |      )})
  |    }
  |    .flatMap() {it}
  |  }
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "${entity.namespace.get().replace('.', '/')}/${entity.className}Validator.groovy"
  }

  String getClassName() {
    entity.className
  }
}
