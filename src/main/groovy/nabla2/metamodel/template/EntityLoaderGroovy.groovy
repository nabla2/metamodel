package nabla2.metamodel.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.excel.ExcelWorkbookLoader
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.Domain
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation

@Canonical
class EntityLoaderGroovy implements TextFileTemplate {

  Domain feature

  String getImportStatements() {
    [ 'groovy.transform.Canonical',
      'io.reactivex.Observable',
      ExcelWorkbookLoader.canonicalName,
      Table.canonicalName,
      ConstraintViolation.canonicalName,
      feature.entities.collect{it.fqn},
      feature.entities.collect{it.fqn + 'Validator'}
    ]
    .flatten()
    .sort()
    .collect {"""\
    |import ${it}
    """.trim()
    }.join('\n')
  }

  String getClassName() {
    "${feature.identifier.className.orElseThrow{new IllegalStateException()}}EntityLoader"
  }

  String getNamespace() {
    "${feature.namespace}.${feature.identifier}"
  }

  @Memoized
  String getSource() {
  """\
  |package ${namespace}
  |
  ${importStatements}
  |
  |/**
  | * ${feature.name}エンティティローダ
  | *
  | * ${regenerationMark}
  | */
  |@Canonical
  |class ${className} {
  |
  |  final Observable<Table> table\$
  |
  |  private ${className}(File file) {
  |    table\$ = ${ExcelWorkbookLoader.simpleName}.from(file).table\$
  |  }
  |
  |  private ${className}(InputStream stream) {
  |    table\$ = ${ExcelWorkbookLoader.simpleName}.from(stream).table\$
  |  }
  |
  |  static ${className} from(File file) {
  |    new ${className}(file)
  |  }
  |
  |  static ${className} from(InputStream stream) {
  |    new ${className}(stream)
  |  }
  |
  ${feature.entities.collect{"""\
  |  Observable<${it.className}> get${it.className}\$() {
  |    ${it.className}.from(table\$)
  |  }
  """.trim()}.join('\n')}
  |
  |  Observable<ConstraintViolation<?>> getConstraintViolation\$() {
  |    Observable.fromArray(
  ${feature.entities.collect{"""\
  |      new ${it.className}Validator().validate(table\$),
  """.trim()}.join('\n')}
  |    ).flatMap{ it }
  |  }
  |
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "${namespace}.${className}".replace('.', '/') + '.groovy'
  }
}
