package nabla2.metamodel.template

import groovy.json.StringEscapeUtils
import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.gradle.GenerateEntityTaskOptionSet
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.JavaIdentifier
import nabla2.metamodel.model.Relationship
import nabla2.metamodel.model.RelationshipProperty
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table

@Canonical
class EntityClassGroovy implements TextFileTemplate {

  Entity entity

  GenerateEntityTaskOptionSet opts

  @Memoized
  String getSource() {
  """\
  ${entity.namespace ? """\
  |package ${entity.namespace}
  |""" : ''}
  ${(importedModules + importedEntities).collect{"""\
  |import ${it}
  """}.join('').trim()
  }
  |/**
  | * ${entity.name}
  | * ${regenerationMark}
  | */
  |@Canonical(excludes = ['_table\$', '_table', '_rowIndex'])
  |class ${entity.className}${traits} {
  |
  |  /** 論理名 */
  |  static final String ENTITY_NAME = "${entity.name}"
  |
  |  // ----- プロパティ定義 ------ //
  |
  ${entity.properties.collect{"""\
  |  /**
  |   * ${it.name}
  |   */
  |  ${it.type.className} ${it.identifier.lowerCamelized.orElse('')}
  """}.join('').trim()}
  |
  |  // ----- 定義元情報 ------ //
  |
  |  private transient Observable<Table> _table\$
  |
  |  private Table _table
  |
  |  Row _row
  |
  |  // ----- メソッド定義 ------ //
  |
  ${(methods - null).join('\n').trim()}
  |}
  """.trim().stripMargin()
  }

  String getTraits() {
    if (entity.traits.empty) return ''
    ' implements ' + entity.traits.collect{ it.trait.className + "<${entity.className}>"}.join(', ')
  }

  @Override
  String getRelPath() {
    entity.namespace.value.orElse('').replace('.', '/') + '/' + entity.identifier.upperCamelized.orElse('') + '.groovy'
  }

  List<String> getMethods() {
    sourcesOfAccessorsThroughRelationship +
    [ sourceOfFrom,
      sourceOfGetFqn,
      sourceOfToString,
    ]
  }

  String getIdentifierTrait() {
    boolean hasIdentifier = entity.properties.any{ it.classMemberName == 'identifier' }
    hasIdentifier ? 'implements JavaIdentifier' : ''
  }

  List<String> getImportedModules() {
    ([
      'groovy.transform.Canonical',
      'groovy.transform.Memoized',
      'io.reactivex.Observable',
      'com.fasterxml.jackson.annotation.JsonIgnore',
      Table.class.canonicalName,
      Row.class.canonicalName,
    ] + entity.properties.collect{ it.type.fqn }
      + entity.traits.collect{ it.trait.fqn }
    ).sort().unique()
  }

  List<String> getImportedEntities() {
    entity
    .relationships
    .collect{ it.referenceEntity }
    .findAll{ it.namespace != entity.namespace }
    .collect{ it.namespace + '.' + it.className }
  }

  List<String> getSourcesOfAccessorsThroughRelationship() {
    entity.relationships.collect{ getSourceOfAccessorsThrough it }
  }

  static String getSourceOfAccessorsThrough(Relationship relationship) {
    String getterName = "get${relationship.identifier.upperCamelized.orElse('')}"
    boolean isListAccessor = relationship.cardinality.value.orElse('')contains('N')
    String className = relationship.referenceEntity.className
    String accessorType = isListAccessor ? "List<$className>" : className
    String toCellOperation = isListAccessor ?
                             "toList().blockingGet()" :
                             "blockingFirst(null)"
    List<RelationshipProperty> props = relationship.referenceProperties

  """\
  |  /**
  |   * ${relationship.name}
  |   */
  |  @Memoized
  |  @JsonIgnore
  |  $accessorType $getterName() {
  |    ${className}.from(_table\$).filter {
  ${props.withIndex().collect { RelationshipProperty rel, i -> """\
  |      this.${rel.property.identifier.lowerCamelized.orElse('')}.sameAs(it.${rel.referenceProperty.identifier.lowerCamelized.orElse('')})${i < props.size()-1 ? ' &&' : ''}
  """}.join('').trim()}
  |    }.${toCellOperation}
  |  }
  """.trim()
  }

  String getSourceOfFrom() {
  """\
  |  /**
  |   * テーブルストリームからエンティティストリームを生成する。
  |   */
  |  @Memoized
  |  static Observable<${entity.className}> from(Observable<Table> table\$) {
  |    table\$
  |    .filter{
  |      it.name.content == Table.OPEN_BRACE_MARK +
  |                         ENTITY_NAME +
  |                         Table.CLOSE_BRACE_MARK
  |    }
  |    .map { table ->
  |      Observable.fromIterable(
  |        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
  |          new ${entity.className}(
  ${entity.properties.collect{ property -> """\
  |            ${property.assignmentJavaSnippet()},
  """.trim()}.join('\n')}
  |            _table\$ : table\$,
  |            _table : table,
  |            _row : table.rows[i],
  |          )
  |        }
  |      )
  |    }
  |    .flatMap {it}
  |    .toList().blockingGet().with{ Observable.fromIterable(it)}
  |  }
  """.trim()
  }

  String getSourceOfGetFqn() {
    if (entity.properties.every{ it.identifier.lowerCamelized.orElse('') != 'namespace' }) {
      return null
    }
  """\
  |  /**
  |   * 完全修飾名
  |   */
  |  @Memoized
  |  String getFqn() {
  |    namespace + (namespace.empty ? '' : '.') + className
  |  }
  |
  |  /**
  |   * クラス名
  |   */
  |  @Memoized
  |  String getClassName() {
  |    identifier.upperCamelized.orElse('')
  |  }
  |
  """.trim()
  }

  static String getSourceOfToString() {
  """\
  |  @Override
  |  String toString() {
  |    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  |  }
  """.trim()
  }
}
