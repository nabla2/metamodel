package nabla2.metamodel.template

import groovy.json.StringEscapeUtils
import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.LocalizedText
import nabla2.metamodel.model.LocalizedTextBundle

@Canonical
class ResourceBundleProperties implements TextFileTemplate {

  LocalizedTextBundle bundle

  @Override
  String getSource() {
  """
  |#
  |# ${bundle.description}
  |# ${regenerationMark}
  |#
  ${bundle.localizedTextList.collect{ entry -> """
  |${entry.key}=${valueOf(entry)}
  """.trim()}.join('\n')}
  """.trim().stripMargin()
  }

  private static String valueOf(LocalizedText entry) {
    entry.content.value.map {
      StringEscapeUtils.escapeJava(it)
    }.orElse('')
  }

  @Override
  String getRelPath() {
    String baseName = bundle.identifier.get()
    bundle.isDefault.truthy \
      ? "${baseName}.properties" \
      : "${baseName}_${bundle.locale}.properties"
  }
}
