package nabla2.metamodel

import groovy.transform.Canonical
import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.generator.model.GeneratorTask
import nabla2.generator.model.GeneratorTaskValidator
import nabla2.generator.model.GeneratorTemplate
import nabla2.generator.model.GeneratorTemplateValidator
import nabla2.metamodel.model.DataType
import nabla2.metamodel.model.DataTypeValidator
import nabla2.metamodel.model.Domain
import nabla2.metamodel.model.DomainValidator
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.EntityTrait
import nabla2.metamodel.model.EntityTraitValidator
import nabla2.metamodel.model.EntityValidator
import nabla2.metamodel.model.ExternalResource
import nabla2.metamodel.model.ExternalResourceValidator
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.GeneratorOptionType
import nabla2.metamodel.model.GeneratorOptionTypeValidator
import nabla2.metamodel.model.GeneratorOptionValidator
import nabla2.metamodel.model.LocalizedText
import nabla2.metamodel.model.LocalizedTextBundle
import nabla2.metamodel.model.LocalizedTextBundleValidator
import nabla2.metamodel.model.LocalizedTextValidator
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.ModelCommonPropertyValidator
import nabla2.metamodel.model.Property
import nabla2.metamodel.model.PropertyValidator
import nabla2.metamodel.model.Relationship
import nabla2.metamodel.model.RelationshipProperty
import nabla2.metamodel.model.RelationshipPropertyValidator
import nabla2.metamodel.model.RelationshipValidator
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.Trait
import nabla2.metamodel.model.TraitValidator
import nabla2.metamodel.model.constraint.ConstraintViolation

/**
 * メタモデルエンティティローダ
 *
 * @author nabla2.metamodel.generator
 */
@Canonical
class MetamodelEntityLoader {

  final Observable<Table> table$

  private MetamodelEntityLoader(File file) {
    table$ = ExcelWorkbookLoader.from(file).table$
  }

  private MetamodelEntityLoader(InputStream stream) {
    table$ = ExcelWorkbookLoader.from(stream).table$
  }

  static MetamodelEntityLoader from(File file) {
    new MetamodelEntityLoader(file)
  }

  static MetamodelEntityLoader from(InputStream stream) {
    new MetamodelEntityLoader(stream)
  }

  Observable<ModelCommonProperty> getModelCommonProperty$() {
    ModelCommonProperty.from(table$)
  }
  Observable<Domain> getDomain$() {
    Domain.from(table$)
  }
  Observable<Entity> getEntity$() {
    Entity.from(table$)
  }
  Observable<ExternalResource> getExternalResource$() {
    ExternalResource.from(table$)
  }
  Observable<Property> getProperty$() {
    Property.from(table$)
  }
  Observable<LocalizedTextBundle> getLocalizedTextBundle$() {
    LocalizedTextBundle.from(table$)
  }
  Observable<LocalizedText> getLocalizedText$() {
    LocalizedText.from(table$)
  }
  Observable<DataType> getDataType$() {
    DataType.from(table$)
  }
  Observable<Relationship> getRelationship$() {
    Relationship.from(table$)
  }
  Observable<RelationshipProperty> getRelationshipProperty$() {
    RelationshipProperty.from(table$)
  }
  Observable<EntityTrait> getEntityTrait$() {
    EntityTrait.from(table$)
  }
  Observable<Trait> getTrait$() {
    Trait.from(table$)
  }
  Observable<GeneratorTemplate> getGeneratorTemplate$() {
    GeneratorTemplate.from(table$)
  }
  Observable<GeneratorTask> getGeneratorTask$() {
    GeneratorTask.from(table$)
  }
  Observable<GeneratorOption> getGeneratorOption$() {
    GeneratorOption.from(table$)
  }
  Observable<GeneratorOptionType> getGeneratorOptionType$() {
    GeneratorOptionType.from(table$)
  }

  Observable<ConstraintViolation<?>> getConstraintViolation$() {
    Observable.fromArray(
      new ModelCommonPropertyValidator().validate(table$),
      new DomainValidator().validate(table$),
      new EntityValidator().validate(table$),
      new ExternalResourceValidator().validate(table$),
      new PropertyValidator().validate(table$),
      new LocalizedTextBundleValidator().validate(table$),
      new LocalizedTextValidator().validate(table$),
      new DataTypeValidator().validate(table$),
      new RelationshipValidator().validate(table$),
      new RelationshipPropertyValidator().validate(table$),
      new EntityTraitValidator().validate(table$),
      new TraitValidator().validate(table$),
      new GeneratorTemplateValidator().validate(table$),
      new GeneratorTaskValidator().validate(table$),
      new GeneratorOptionValidator().validate(table$),
      new GeneratorOptionTypeValidator().validate(table$),
    ).flatMap{ it }
  }

}