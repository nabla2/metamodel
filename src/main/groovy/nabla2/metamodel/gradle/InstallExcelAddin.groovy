package nabla2.metamodel.gradle

import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.excel.addin.template.ExcelAddinManifestXml
import nabla2.excel.addin.template.IndexHtml
import nabla2.excel.addin.template.MainTsx
import nabla2.excel.addin.template.PackageJson
import nabla2.excel.addin.template.TsconfigJson
import nabla2.excel.addin.template.WebpackConfigJs
import nabla2.generator.model.GeneratorTaskValidator
import nabla2.generator.model.GeneratorTemplateValidator
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.DataTypeValidator
import nabla2.metamodel.model.Domain
import nabla2.metamodel.model.DomainValidator
import nabla2.metamodel.model.EntityTraitValidator
import nabla2.metamodel.model.EntityValidator
import nabla2.metamodel.model.ExternalResourceValidator
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.GeneratorOptionTypeValidator
import nabla2.metamodel.model.GeneratorOptionValidator
import nabla2.metamodel.model.LocalizedTextBundleValidator
import nabla2.metamodel.model.LocalizedTextValidator
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.ModelCommonPropertyValidator
import nabla2.metamodel.model.PropertyValidator
import nabla2.metamodel.model.RelationshipPropertyValidator
import nabla2.metamodel.model.RelationshipValidator
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.TraitValidator
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
/**
 * Excelアドインを展開
 *
 * @author nabla2.metamodel.generator
 */
class InstallExcelAddin extends DefaultTask {

  @InputFiles FileCollection workbooks
  @InputFiles @Optional FileCollection referringTo
  @OutputDirectory @Optional File excelAddinManifestXmlDir
  @OutputDirectory @Optional File mainTsxDir
  File baseDir
  @OutputFile @Optional File errorReport
  String group = 'laplacian generator'
  String description = 'モデルの整合性をリアルタイム表示するExcelアドインをローカルインストールする。'

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with { _table$ ->
      errorReport.text = ''
      Throwable error = null
      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table$).toList().blockingGet().inject([:]) { acc, prop ->
        Map<String,String> bucket = acc[prop.entityName.get()]
        if (prop.value.empty) return acc
        if (!bucket) {
          bucket = [:]
          acc[prop.entityName.get()] = bucket
        }
        bucket[prop.propertyName.get()] = prop.value.orElse('')
        acc
      }
      def table$ = _table$.map{ table ->
        table.copyWith(globalMetadata: commonProps)
      }
      validate(table$)
      List<GeneratorOption> optionList = GeneratorOption.from(table$).filter{
        it.optionType.targetTask.identifier.sameAs('InstallExcelAddin')
      }.toList().blockingGet()
      Domain.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ExcelAddinManifestXml(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(System.getProperty("user.home"), '/Library/Containers/com.microsoft.Excel/Data/Documents/wef'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Domain.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new PackageJson(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Domain.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new TsconfigJson(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Domain.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WebpackConfigJs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Domain.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new IndexHtml(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Domain.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new MainTsx(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
    }
  }

  void validate(Observable<Table> table$) {
    Throwable error
    println 'Checking model files...'
    Observable.fromArray(
      new ModelCommonPropertyValidator().validate(table$),
      new DomainValidator().validate(table$),
      new EntityValidator().validate(table$),
      new ExternalResourceValidator().validate(table$),
      new PropertyValidator().validate(table$),
      new LocalizedTextBundleValidator().validate(table$),
      new LocalizedTextValidator().validate(table$),
      new DataTypeValidator().validate(table$),
      new RelationshipValidator().validate(table$),
      new RelationshipPropertyValidator().validate(table$),
      new EntityTraitValidator().validate(table$),
      new TraitValidator().validate(table$),
      new GeneratorTemplateValidator().validate(table$),
      new GeneratorTaskValidator().validate(table$),
      new GeneratorOptionValidator().validate(table$),
      new GeneratorOptionTypeValidator().validate(table$),
    )
    .flatMap {it}
    .doOnNext{ println it; errorReport << it }
    .doOnError{ it.printStackTrace(); error = it }
    .onErrorResumeNext(Observable.empty())
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new GradleException(
        "There are ${it.size()} errors in model files. see details in ${this.errorReport.path}"
      )
      if (error) throw new GradleException(
        "An error occurred while validating the model files.", error
      )
    }
  }

  static class Settings extends TaskSettings<InstallExcelAddin> {
    FileCollection workbooks
    FileCollection referringTo
    File baseDir
    File errorReport
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(InstallExcelAddin task) {
      File baseDir = this.baseDir ?: new File('./')
      task.workbooks = workbooks
      task.referringTo = referringTo ?: project.files([])
      task.baseDir = baseDir
      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
      task.excelAddinManifestXmlDir = new File(baseDir, '~/Library/Containers/com.microsoft.Excel/Data/Documents/wef')
      task.mainTsxDir = new File(baseDir, 'src')
    }
  }
}