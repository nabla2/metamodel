package nabla2.metamodel.gradle

import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.generator.model.GeneratorTaskValidator
import nabla2.generator.model.GeneratorTemplateValidator
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.DataTypeValidator
import nabla2.metamodel.model.Domain
import nabla2.metamodel.model.DomainValidator
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.EntityTraitValidator
import nabla2.metamodel.model.EntityValidator
import nabla2.metamodel.model.ExternalResourceValidator
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.GeneratorOptionTypeValidator
import nabla2.metamodel.model.GeneratorOptionValidator
import nabla2.metamodel.model.LocalizedTextBundleValidator
import nabla2.metamodel.model.LocalizedTextValidator
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.ModelCommonPropertyValidator
import nabla2.metamodel.model.PropertyValidator
import nabla2.metamodel.model.RelationshipPropertyValidator
import nabla2.metamodel.model.RelationshipValidator
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.TraitValidator
import nabla2.metamodel.template.EntityClassGroovy
import nabla2.metamodel.template.EntityLoaderGroovy
import nabla2.metamodel.template.EntityValidatorGroovy
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
/**
 * エンティティクラスを生成
 *
 * @author nabla2.metamodel.generator
 */
class GenerateEntityTask extends DefaultTask {

  @InputFiles FileCollection workbooks
  @InputFiles @Optional FileCollection referringTo
  @OutputDirectory @Optional File entityClassGroovyDir
  @OutputDirectory @Optional File entityValidatorGroovyDir
  @OutputDirectory @Optional File entityLoaderGroovyDir
  File baseDir
  @OutputFile @Optional File errorReport
  String group = 'laplacian generator'
  String description = '各種自動生成の元情報となるモデルクラス(Groovy)を生成する。'

  String getEnv() {
    project.properties.find{it.key.toLowerCase() == 'env'}.with {
      (it == null || it.value.toString().trim().empty) ? '' : it.value.toString().toUpperCase()
    }
  }

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with { _table$ ->
      errorReport.text = ''
      Throwable error = null
      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table$).toList().blockingGet().inject([:]) { acc, prop ->
        Map<String,String> bucket = acc[prop.entityName.get()]
        if (prop.value.empty) return acc
        if (!bucket) {
          bucket = [:]
          acc[prop.entityName.get()] = bucket
        }
        bucket[prop.propertyName.get()] = prop.value.orElse('')
        acc
      }
      def table$ = _table$.map{ table ->
        table.copyWith(globalMetadata: commonProps)
      }
      validate(table$)
      List<GeneratorOption> optionList = GeneratorOption.from(table$).filter{
        it.optionType.targetTask.identifier.sameAs('GenerateEntityTask')
      }.toList().blockingGet()
      Entity.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new EntityClassGroovy(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/groovy'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Entity.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new EntityValidatorGroovy(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/groovy'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Domain.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new EntityLoaderGroovy(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/groovy'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
    }
  }

  void validate(Observable<Table> table$) {
    Throwable error
    println 'Checking model files...'
    Observable.fromArray(
      new ModelCommonPropertyValidator().validate(table$),
      new DomainValidator().validate(table$),
      new EntityValidator().validate(table$),
      new ExternalResourceValidator().validate(table$),
      new PropertyValidator().validate(table$),
      new LocalizedTextBundleValidator().validate(table$),
      new LocalizedTextValidator().validate(table$),
      new DataTypeValidator().validate(table$),
      new RelationshipValidator().validate(table$),
      new RelationshipPropertyValidator().validate(table$),
      new EntityTraitValidator().validate(table$),
      new TraitValidator().validate(table$),
      new GeneratorTemplateValidator().validate(table$),
      new GeneratorTaskValidator().validate(table$),
      new GeneratorOptionValidator().validate(table$),
      new GeneratorOptionTypeValidator().validate(table$),
    )
    .flatMap {it}
    .doOnNext{ println it; errorReport << it }
    .doOnError{ it.printStackTrace(); error = it }
    .onErrorResumeNext(Observable.empty())
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new GradleException(
        "There are ${it.size()} errors in model files. see details in ${this.errorReport.path}"
      )
      if (error) throw new GradleException(
        "An error occurred while validating the model files.", error
      )
    }
  }

  static class Settings extends TaskSettings<GenerateEntityTask> {
    FileCollection workbooks
    FileCollection referringTo
    File baseDir
    File errorReport
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(GenerateEntityTask task) {
      File baseDir = this.baseDir ?: new File('./')
      task.workbooks = workbooks
      task.referringTo = referringTo ?: project.files([])
      task.baseDir = baseDir
      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
      task.entityClassGroovyDir = new File(baseDir, 'src/main/groovy')
      task.entityValidatorGroovyDir = new File(baseDir, 'src/main/groovy')
      task.entityLoaderGroovyDir = new File(baseDir, 'src/main/groovy')
    }
  }
}