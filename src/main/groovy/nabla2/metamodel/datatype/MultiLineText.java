package nabla2.metamodel.datatype;

import java.util.Optional;
import java.util.regex.Pattern;

public class MultiLineText extends DefinedWithRegex<String> {

    private static final Pattern PATTERN = Pattern.compile(
    "[\\s\\S]*"
    );

    public MultiLineText(String literal) {
        super(literal);
    }

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    @Override
    public String getInvalidMessage() {
        return "Invalid multi-line text";
    }

    @Override
    public Optional<String> getValue() {
        return getLiteral();
    }
}
