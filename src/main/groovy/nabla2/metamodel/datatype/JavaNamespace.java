package nabla2.metamodel.datatype;

import java.util.Optional;
import java.util.regex.Pattern;

public class JavaNamespace extends DefinedWithRegex<String> {

    public JavaNamespace(String literal) {
        super(literal);
    }

    @Override
    public String getInvalidMessage() {
        return "invalid namespace: " + getLiteral();
    }

    private static final Pattern PATTERN = Pattern.compile(
    "([a-zA-Z_$][a-zA-Z0-9_$]*)(\\.[a-zA-Z_$][a-zA-Z0-9_$]*)*"
    );

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    @Override
    public Optional<String> getValue() {
        return getLiteral();
    }
}
