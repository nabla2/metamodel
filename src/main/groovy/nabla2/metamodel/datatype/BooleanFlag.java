package nabla2.metamodel.datatype;

import java.util.Optional;
import java.util.regex.Pattern;

public class BooleanFlag extends DefinedWithRegex<Boolean> {

    public BooleanFlag(String value) {
        super(value);
    }

    @Override
    public String getInvalidMessage() {
        return "Only following values are permitted: Y(es) or N(o) / T(rue) or F(alse) / ○ or ☓ / ON or OFF";
    }

    private static final Pattern PATTERN = Pattern.compile(
    "([TFYN○☓]|ON|OFF|YES|NO|TRUE|FALSE)", Pattern.CASE_INSENSITIVE
    );

    private static final Pattern TRUTHY = Pattern.compile(
    "(Y|YES|T|TRUE|ON|○)", Pattern.CASE_INSENSITIVE
    );

    private static final Pattern FALSY = Pattern.compile(
    "(N|NO|F|FALSE|OFF|☓)", Pattern.CASE_INSENSITIVE
    );

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    public boolean isTruthy() {
        return getLiteral()
              .map(BooleanFlag::truthy)
              .orElse(false);
    }

    public boolean isFalsy() {
        return getLiteral()
              .map(BooleanFlag::falsy)
              .orElse(false);
    }

    public static boolean truthy(String value) {
        return (value != null)
            && TRUTHY.matcher(value).matches();
    }

    public static boolean falsy(String value) {
        return (value != null)
            && FALSY.matcher(value).matches();
    }

    @Override
    public Optional<Boolean> getValue() {
        return getLiteral().flatMap(l -> {
            return truthy(l) ? Optional.of(true)
                 : falsy(l)  ? Optional.of(false)
                 : Optional.empty();
        });
    }
}
