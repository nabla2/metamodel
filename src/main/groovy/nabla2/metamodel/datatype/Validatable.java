package nabla2.metamodel.datatype;

import java.util.Optional;

public interface Validatable {

  boolean isValid();

  String getInvalidMessage();

  default Optional<String> getMessageIfInvalid() {
      return isValid()
           ? Optional.empty()
           : Optional.of(getInvalidMessage());
  }
}
