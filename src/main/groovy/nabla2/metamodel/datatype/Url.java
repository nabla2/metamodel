package nabla2.metamodel.datatype;

import java.util.Optional;
import java.util.regex.Pattern;

public class Url extends DefinedWithRegex<String> {

    private static final Pattern PATTERN = Pattern.compile(
    "([a-z0-9]+:(//)?[^./?#]+(\\.[^./?#]+)*)?(/[^/?#]+)*/?(\\?[^?&#=]+=[^?&#=]*(&[^?&#=]+=[^?&#=]*)*)?(#[^#]*)?"
    );

    public Url(String value) {
        super(value);
    }

    @Override
    public String getInvalidMessage() {
        return "Malformed URL";
    }

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    @Override
    public Optional<String> getValue() {
        return getLiteral();
    }
}
