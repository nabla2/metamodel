package nabla2.metamodel.datatype;

import groovy.lang.Closure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlWithPlaceholder extends DefinedWithRegex<Pattern> {

    private final Pattern formatPattern;

    private static final Pattern PATTERN = Pattern.compile(
    "([a-z0-9]+:(//)?[^./?#]+(\\.[^./?#]+)*)?(/([^{}/?#]+|\\{[^{}/?#]+}))*/?(\\?[^?&#=]+=[^?&#=]*(&[^?&#=]+=[^?&#=]*)*)?(#[^#]*)?"
    );

    public UrlWithPlaceholder(String value) {
        super(value);
        this.formatPattern = (value == null || !isValid())
                           ? null
                           :patternOf(value);
    }

    private static Pattern patternOf(String value) {
      return Pattern.compile("\\Q" + value.replaceAll(
          "\\{[^}]+}", "\\\\E([^/]+)\\\\Q"
      ) + "\\E");
    }

    @Override
    public String getInvalidMessage() {
        return "Malformed URL";
    }

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    @Override
    public Optional<Pattern> getValue() {
        return Optional.ofNullable(formatPattern);
    }

    @Override
    public boolean sameAs(LiteralType<?> another) {
        return getValue()
        .map(p -> another
                 .getLiteral()
                 .map(l -> p.matcher(l).matches())
                 .orElse(false))
        .orElse(!another.getLiteral().isPresent());
    }

    public boolean sameAs(String another) {
        return another == null
            || another.isEmpty()
            || getValue()
               .map(p -> p.matcher(another).matches())
               .orElse(false);
    }

    public List<String> capture(String url) {
        if (url == null) return Collections.emptyList();
        return getValue()
        .map(pattern -> {
            Matcher m = pattern.matcher(url);
            if (!m.matches()) return Collections.<String>emptyList();
            if (m.groupCount() == 0) return Collections.singletonList(m.group(0));
            List<String> results = new ArrayList<>();
            for (int i = 1; i <= m.groupCount(); i++) {
                results.add(m.group(i));
            }
            return results;
        })
        .orElse(Collections.emptyList());
    }

    public List<String> getEmbededParamNames() {
        List<String> names = new ArrayList<>();
        embedParams(name -> {
            names.add(name);
            return "";
        });
        return names;
    }

    /**
     * この文字列のパラメータ部分を置換した文字列を返す。
     * @param replacer 置換処理
     * @return 置換後の文字列
     */
    public Optional<String> embedParams(Function<String, String> replacer) {
        return getLiteral().map(l -> {
            Matcher m = PH.matcher(l);
            StringBuffer result = new StringBuffer();
            while (m.find()) {
                String replacement = replacer.apply(m.group(1));
                if (replacement == null) throw new IllegalArgumentException(
                "Invalid url placeholder: " + this.getLiteral()
                );
                m.appendReplacement(result, replacement);
            }
            m.appendTail(result);
            return result.toString();
        });
    }

    private static final Pattern PH = Pattern.compile(
        "\\{([^}/]+)\\}"
    );
}
