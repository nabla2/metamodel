package nabla2.metamodel.datatype;

import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StrictJavaIdentifier extends DefinedWithRegex<String> {

    private static final Pattern PATTERN = Pattern.compile(
        "^[_$a-zA-Z][_$0-9a-zA-Z]*$"
    );

    public Pattern getPattern() {
        return PATTERN;
    }

    public StrictJavaIdentifier(String value) {
        super(value);
    }

    public String getInvalidMessage() {
        return "Only alphabet, digit and two symbols ('$' and '_') are allowed.";
    }

    @Override
    public Optional<String> getValue() {
        return getLiteral();
    }

    public static String normalize(String name) {
        return (name == null)
             ? null
             : name.replaceAll("[^._0-9a-zA-Z$\\s]+", "");
    }

    public static String upperCamelize(String name) {
        return (name == null) ? null
             : Pattern.compile("[_\\s]+")
              .splitAsStream(normalize(name))
              .map(it -> it.substring(0, 1).toUpperCase() + it.substring(1))
              .collect(Collectors.joining());
    }

    public static String lowerCamelize(String name) {
        return (name == null) ? null
             : Stream.of(upperCamelize(name))
              .map(it -> it.substring(0, 1).toLowerCase() + it.substring(1))
              .collect(Collectors.joining());
    }

    public static String dasherize(String name) {
        return (name == null) ? null
             : Pattern.compile("[_\\s]+")
              .splitAsStream(normalize(name).toLowerCase())
              .collect(Collectors.joining("-"));
    }

    public static String underscorize(String name) {
        return (name == null) ? null
             : Pattern.compile("[_\\s]+")
              .splitAsStream(normalize(name).toLowerCase())
              .collect(Collectors.joining("_"));
    }

    public Optional<String> getUpperCamelized() {
        return getValue()
              .map(StrictJavaIdentifier::upperCamelize);
    }

    public Optional<String> getLowerCamelized() {
        return getValue()
              .map(StrictJavaIdentifier::lowerCamelize);
    }

    public Optional<String> getDasherized() {
        return getValue()
              .map(StrictJavaIdentifier::dasherize);
    }

    public Optional<String> getUnderscorized() {
        return getValue()
              .map(StrictJavaIdentifier::underscorize);
    }

    public Optional<String> getClassName() {
        return this.getUpperCamelized();
    }

    public Optional<String> getClassMemberName() {
        return this.getLowerCamelized();
    }
}
