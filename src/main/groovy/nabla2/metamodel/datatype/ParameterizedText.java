package nabla2.metamodel.datatype;

import groovy.lang.Tuple2;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParameterizedText extends DefinedWithRegex<Pattern> {

    private final Pattern formatPattern;

    public ParameterizedText(String value) {
        super(value);
        formatPattern = buildFormatPatternFrom(value);
    }

    private Pattern buildFormatPatternFrom(String value) {
        if (value == null) return null;
        return Pattern.compile(
            ("\\Q"
            + value
             .replaceAll("\\s*【(:[^【】]*|[^【:】]*)】\\s*", "\\\\E\\\\s*【(:[^【】]*|[^【:】]*)】\\\\s*\\\\Q")
             .replaceAll("\\s*【[^【:】]+:[^【】]*】\\s*", "\\\\E(?:\\\\s*【([^【:】]+(?:\\:[^【】]*)?)】\\\\s*)?\\\\Q")
            + "\\E"
            ).replaceAll("\\\\Q\\\\E", "")
            + "(?:\\s*【[^【:】]+:[^【】]*】\\s*)*?"
        );
    }

    @Override
    public String getInvalidMessage() {
        return "Invalid parameterized text's format: " + getLiteral();
    }

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    private static final Pattern PATTERN = Pattern.compile(
    "^([^【】]*【[^【】]*】)*[^【】]*$"
    );

    @Override
    public Optional<Pattern> getValue() {
        return Optional.ofNullable(this.formatPattern);
    }

    public List<String> getParams() {
        return getLiteral()
              .map(this::capture)
              .orElse(Collections.emptyList());
    }

    public List<String> capture(String text) {
        if (text == null) return Collections.emptyList();
        return getValue()
        .map(pattern -> {
            Matcher m = pattern.matcher(text);
            if (!m.matches()) return Collections.<String>emptyList();
            List<String> names = new ArrayList<>(m.groupCount());
            for (int i = 0; i < m.groupCount(); i++) {
                String v =  m.group(i + 1);
                names.add((v == null) ? null : v.replaceAll("^[:]", ""));
            }
            return names;
        })
        .orElse(Collections.emptyList());
    }

    public Map<String, String> captureOptions(String text) {
        Map<String, String> params = getParams().stream().collect(Collectors.toMap(
           p -> p.replaceAll(":.*$", ""),
           p -> p.replaceAll("^[^:]+:", "")
        ));

        return capture(text).stream()
        .filter(p -> p != null && p.contains(":"))
        .collect(Collectors.toMap(
           p -> params.get(p.replaceAll(":.*$", "")),
           p -> p.replaceAll("^[^:]+:", "")
        ));
    }

    public List<Map.Entry<String, String>> captureWithName(String text) {
        List<String> params = getParams();
        List<String> values = capture(text);
        int len = values.size();
        return IntStream.range(0, params.size())
        .mapToObj(i -> {
            return new AbstractMap.SimpleEntry<String, String>(
                params.get(i), (i < len) ? values.get(i) : null
            );
        })
        .collect(Collectors.toList());
    }

    /**
     * この文字列のパラメータ部分を置換した文字列を返す。
     * @param replacer 置換処理
     * @return 置換後の文字列
     */
    public Optional<String> embedParams(Function<String, String> replacer) {
        return getLiteral().map(l -> {
            Matcher m = PH.matcher(l);
            StringBuffer result = new StringBuffer();
            while (m.find()) {
                Object replacement = replacer.apply(m.group(1));
                if (replacement == null) throw new IllegalArgumentException(
                "Invalid placeholder: " + this.getLiteral()
                );
                m.appendReplacement(result, replacement.toString());
            }
            m.appendTail(result);
            return result.toString();
        });
    }

    private static final Pattern PH = Pattern.compile(
        "【([^】]+)】"
    );

    @Override
    public boolean sameAs(LiteralType<?> another) {
        return getValue()
        .map(pattern -> {
            return another
                  .getLiteral()
                  .map(l -> pattern.matcher(l).matches())
                  .orElse(false);
        })
        .orElse(false);
    }

    public boolean sameAs(String another) {
        return another == null
            || another.isEmpty()
            || getValue()
               .map(p -> p.matcher(another).matches())
               .orElse(false);
    }
}
