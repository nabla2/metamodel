package nabla2.metamodel.datatype;

import java.util.Optional;
import java.util.regex.Pattern;

public class OrdinalNumber extends DefinedWithRegex<Integer> {

    public OrdinalNumber(String literal) {
        super(literal);
    }

    @Override
    public String getInvalidMessage() {
        return "must be a unsigned digits (zero is not allowed)";
    }

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    private static final Pattern PATTERN = Pattern.compile(
    "[1-9][0-9]*"
    );

    @Override
    public Optional<Integer> getValue() {
        return getLiteral().map(Integer::valueOf);
    }
}
