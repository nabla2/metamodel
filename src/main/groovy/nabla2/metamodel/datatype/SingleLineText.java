package nabla2.metamodel.datatype;

import java.util.Optional;
import java.util.regex.Pattern;

public class SingleLineText extends DefinedWithRegex<String> {

    public SingleLineText(String value) {
        super(value);
    }

    @Override
    public String getInvalidMessage() {
        return "This value can not include any line break characters: " + getLiteral();
    }

    public Pattern getPattern() {
        return PATTERN;
    }

    private static final Pattern PATTERN = Pattern.compile(
    "^[^\r\n]*$"
    );

    @Override
    public Optional<String> getValue() {
        return getLiteral();
    }
}
