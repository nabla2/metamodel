package nabla2.metamodel.datatype;

import java.io.Serializable;
import java.util.Optional;
import java.util.regex.Pattern;

public interface LiteralType<T> extends Validatable, Serializable {

    Optional<String> getLiteral();

    Optional<T> getValue();

    default T get() {
        return getValue()
              .orElseThrow(IllegalStateException::new);
    }

    default boolean sameAs(LiteralType<?> another) {
        Optional<T> v = this.getValue();
        Optional<?> o = another.getValue();
        if (!o.isPresent()) {
            return !v.isPresent();
        }
        if (o.get() instanceof Pattern) {
            return getLiteral()
                  .map(l -> ((Pattern) o.get()).matcher(l).matches())
                  .orElse(true);
        }
        return getValue()
              .equals(another.getValue());
    }

    default boolean sameAs(T another) {
        if (another instanceof Pattern) {
            return getLiteral()
                  .map(l -> ((Pattern) another).matcher(l).matches())
                  .orElse(true);
        }
        return getValue()
              .map(v -> v.equals(another))
              .orElse(another == null);
    }

    default boolean isEmpty() {
        return getLiteral()
              .map(String::isEmpty)
              .orElse(true);
    }
}
