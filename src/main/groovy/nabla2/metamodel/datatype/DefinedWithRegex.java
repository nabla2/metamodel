package nabla2.metamodel.datatype;

import java.util.Optional;
import java.util.regex.Pattern;

public abstract class DefinedWithRegex<T> implements LiteralType<T> {

    private final String literal;

    public DefinedWithRegex(String literal) {
        this.literal = literal;
    }

    @Override
    public Optional<String> getLiteral() {
        return Optional.ofNullable(literal);
    }

    public abstract Pattern getPattern();

    public abstract String getInvalidMessage();

    public boolean isValid() {
        return getLiteral().map(
            l -> l.isEmpty() || getPattern().matcher(l).matches()
        ).orElse(true);
    }

    @Override
    public String toString() {
        return getLiteral().orElse("");
    }

    public String plus(String str) {
        return toString() + str;
    }
}
