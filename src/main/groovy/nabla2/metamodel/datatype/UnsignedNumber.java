package nabla2.metamodel.datatype;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.regex.Pattern;

public class UnsignedNumber extends DefinedWithRegex<BigDecimal> {

    public UnsignedNumber(String value) {
        super(value);
    }

    @Override
    public String getInvalidMessage() {
        return "must be an unsigned number: " + getLiteral();
    }

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    private static final Pattern PATTERN = Pattern.compile(
    "^([1-9][0-9]*|0)(\\.[0-9]+)?$"
    );

    @Override
    public Optional<BigDecimal> getValue() {
        return getLiteral().map(BigDecimal::new);
    }
}
