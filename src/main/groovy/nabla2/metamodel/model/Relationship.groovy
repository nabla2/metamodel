package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.RelationshipProperty
/**
 * リレーション
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Relationship {

  /** 論理名 */
  static final String ENTITY_NAME = "リレーション"

  // ----- プロパティ定義 ------ //

  /**
   * エンティティ名
   */
  SingleLineText entityName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 参照エンティティ名
   */
  SingleLineText referenceEntityName
  /**
   * 多重度
   */
  SingleLineText cardinality

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 参照エンティティ
   */
  @Memoized
  @JsonIgnore
  Entity getReferenceEntity() {
    Entity.from(_table$).filter {
      this.referenceEntityName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 参照プロパティリスト
   */
  @Memoized
  @JsonIgnore
  List<RelationshipProperty> getReferenceProperties() {
    RelationshipProperty.from(_table$).filter {
      this.name.sameAs(it.relationshipName) &&
      this.entityName.sameAs(it.entityName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Relationship> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Relationship(
            entityName : new SingleLineText(row['エンティティ名']),
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            referenceEntityName : new SingleLineText(row['参照エンティティ名']),
            cardinality : new SingleLineText(row['多重度']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}