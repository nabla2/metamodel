package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 外部リソースバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ExternalResourceValidator implements Validator<ExternalResource> {
  Observable<ConstraintViolation<ExternalResource>> validate(Observable<Table> table$) {
    Observable<ExternalResource> entity$ = ExternalResource.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<ExternalResource>> validateUniqueConstraint(Observable<ExternalResource> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      ExternalResource another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<ExternalResource> duplicated = it.get('__duplicated__')
      new ConstraintViolation<ExternalResource>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '名称',
          '外部リソース',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<ExternalResource>> validateCardinalityConstraint(Observable<ExternalResource> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ExternalResource>> validatePropertyType(Observable<ExternalResource> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.path.messageIfInvalid.map{['パス', it]}.orElse(null),
        entity.localPath.messageIfInvalid.map{['ローカルパス', it]}.orElse(null),
        entity.os.messageIfInvalid.map{['OS', it]}.orElse(null),
        entity.architecture.messageIfInvalid.map{['アーキテクチャ', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<ExternalResource>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ExternalResource>> validateRequiredProperty(Observable<ExternalResource> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.literal.map{''}.orElse('名称'),
        entity.path.literal.map{''}.orElse('パス'),
        entity.localPath.literal.map{''}.orElse('ローカルパス'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<ExternalResource>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}