package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 領域バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class DomainValidator implements Validator<Domain> {
  Observable<ConstraintViolation<Domain>> validate(Observable<Table> table$) {
    Observable<Domain> entity$ = Domain.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Domain>> validateUniqueConstraint(Observable<Domain> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      Domain another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<Domain> duplicated = it.get('__duplicated__')
      new ConstraintViolation<Domain>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '名称',
          '領域',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<Domain>> validateCardinalityConstraint(Observable<Domain> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Domain>> validatePropertyType(Observable<Domain> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.namespace.messageIfInvalid.map{['名前空間', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Domain>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Domain>> validateRequiredProperty(Observable<Domain> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.namespace.literal.map{''}.orElse('名前空間'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Domain>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}