package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * モデル共通プロパティバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ModelCommonPropertyValidator implements Validator<ModelCommonProperty> {
  Observable<ConstraintViolation<ModelCommonProperty>> validate(Observable<Table> table$) {
    Observable<ModelCommonProperty> entity$ = ModelCommonProperty.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<ModelCommonProperty>> validateUniqueConstraint(Observable<ModelCommonProperty> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        entityName : entity.entityName.value.map{it.toString()}.orElse(null),
        propertyName : entity.propertyName.value.map{it.toString()}.orElse(null),
        environment : entity.environment.value.map{it.toString()}.orElse(null),
      ])
      ModelCommonProperty another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<ModelCommonProperty> duplicated = it.get('__duplicated__')
      new ConstraintViolation<ModelCommonProperty>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'エンティティ名, プロパティ名, 環境',
          'モデル共通プロパティ',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<ModelCommonProperty>> validateCardinalityConstraint(Observable<ModelCommonProperty> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ModelCommonProperty>> validatePropertyType(Observable<ModelCommonProperty> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.entityName.messageIfInvalid.map{['エンティティ名', it]}.orElse(null),
        entity.propertyName.messageIfInvalid.map{['プロパティ名', it]}.orElse(null),
        entity.environment.messageIfInvalid.map{['環境', it]}.orElse(null),
        entity.value.messageIfInvalid.map{['値', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<ModelCommonProperty>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ModelCommonProperty>> validateRequiredProperty(Observable<ModelCommonProperty> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.entityName.literal.map{''}.orElse('エンティティ名'),
        entity.propertyName.literal.map{''}.orElse('プロパティ名'),
        entity.environment.literal.map{''}.orElse('環境'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<ModelCommonProperty>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}