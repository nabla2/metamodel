package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.trait.LocalizedTextTrait
import nabla2.metamodel.model.LocalizedTextBundle
/**
 * 多言語化テキスト
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class LocalizedText implements LocalizedTextTrait<LocalizedText> {

  /** 論理名 */
  static final String ENTITY_NAME = "多言語化テキスト"

  // ----- プロパティ定義 ------ //

  /**
   * 区分名
   */
  SingleLineText bundleName
  /**
   * ロケール
   */
  SingleLineText locale
  /**
   * キー
   */
  JavaNamespace key
  /**
   * 内容
   */
  MultiLineText content
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 区分
   */
  @Memoized
  @JsonIgnore
  LocalizedTextBundle getBundle() {
    LocalizedTextBundle.from(_table$).filter {
      this.bundleName.sameAs(it.name) &&
      this.locale.sameAs(it.locale)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<LocalizedText> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new LocalizedText(
            bundleName : new SingleLineText(row['区分名']),
            locale : new SingleLineText(row['ロケール']),
            key : new JavaNamespace(row['キー']),
            content : new MultiLineText(row['内容']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}