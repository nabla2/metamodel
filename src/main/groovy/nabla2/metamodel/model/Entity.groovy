package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.trait.TraitOfEntity
import nabla2.metamodel.model.Property
import nabla2.metamodel.model.Relationship
import nabla2.metamodel.model.EntityTrait
import nabla2.metamodel.model.Domain
/**
 * エンティティ
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Entity implements TraitOfEntity<Entity> {

  /** 論理名 */
  static final String ENTITY_NAME = "エンティティ"

  // ----- プロパティ定義 ------ //

  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 名前空間
   */
  JavaNamespace namespace
  /**
   * 領域名
   */
  SingleLineText domainName

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * プロパティリスト
   */
  @Memoized
  @JsonIgnore
  List<Property> getProperties() {
    Property.from(_table$).filter {
      this.name.sameAs(it.entityName)
    }.toList().blockingGet()
  }
  /**
   * リレーションリスト
   */
  @Memoized
  @JsonIgnore
  List<Relationship> getRelationships() {
    Relationship.from(_table$).filter {
      this.name.sameAs(it.entityName)
    }.toList().blockingGet()
  }
  /**
   * トレイトリスト
   */
  @Memoized
  @JsonIgnore
  List<EntityTrait> getTraits() {
    EntityTrait.from(_table$).filter {
      this.name.sameAs(it.entityName)
    }.toList().blockingGet()
  }
  /**
   * 領域
   */
  @Memoized
  @JsonIgnore
  Domain getDomain() {
    Domain.from(_table$).filter {
      this.domainName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Entity> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Entity(
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            namespace : new JavaNamespace(row['名前空間']),
            domainName : new SingleLineText(row['領域名']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  /**
   * 完全修飾名
   */
  @Memoized
  String getFqn() {
    namespace + (namespace.empty ? '' : '.') + className
  }

  /**
   * クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.orElse('')
  }

  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}