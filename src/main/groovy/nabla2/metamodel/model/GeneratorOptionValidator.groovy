package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 自動生成オプションバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class GeneratorOptionValidator implements Validator<GeneratorOption> {
  Observable<ConstraintViolation<GeneratorOption>> validate(Observable<Table> table$) {
    Observable<GeneratorOption> entity$ = GeneratorOption.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<GeneratorOption>> validateUniqueConstraint(Observable<GeneratorOption> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        environment : entity.environment.value.map{it.toString()}.orElse(null),
        optionTypeName : entity.optionTypeName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      GeneratorOption another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<GeneratorOption> duplicated = it.get('__duplicated__')
      new ConstraintViolation<GeneratorOption>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '環境, オプション種別名, 名称',
          '自動生成オプション',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<GeneratorOption>> validateCardinalityConstraint(Observable<GeneratorOption> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.optionType == null) {
        violations.add(new ConstraintViolation<GeneratorOption>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '自動生成オプション種別',
            "名称=${entity.optionTypeName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<GeneratorOption>> validatePropertyType(Observable<GeneratorOption> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.environment.messageIfInvalid.map{['環境', it]}.orElse(null),
        entity.optionTypeName.messageIfInvalid.map{['オプション種別名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.value.messageIfInvalid.map{['値', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<GeneratorOption>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<GeneratorOption>> validateRequiredProperty(Observable<GeneratorOption> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.environment.literal.map{''}.orElse('環境'),
        entity.optionTypeName.literal.map{''}.orElse('オプション種別名'),
        entity.name.literal.map{''}.orElse('名称'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<GeneratorOption>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}