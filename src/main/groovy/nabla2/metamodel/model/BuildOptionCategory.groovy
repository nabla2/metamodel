package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.BuildOption
/**
 * ビルドオプション区分
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class BuildOptionCategory {

  /** 論理名 */
  static final String ENTITY_NAME = "ビルドオプション区分"

  // ----- プロパティ定義 ------ //

  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 說明
   */
  MultiLineText value

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * オプションリスト
   */
  @Memoized
  @JsonIgnore
  List<BuildOption> getOptions() {
    BuildOption.from(_table$).filter {
      this.name.sameAs(it.categoryName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<BuildOptionCategory> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new BuildOptionCategory(
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            value : new MultiLineText(row['說明']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}