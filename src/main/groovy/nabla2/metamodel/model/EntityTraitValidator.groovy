package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * エンティティ-トレイトバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class EntityTraitValidator implements Validator<EntityTrait> {
  Observable<ConstraintViolation<EntityTrait>> validate(Observable<Table> table$) {
    Observable<EntityTrait> entity$ = EntityTrait.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<EntityTrait>> validateUniqueConstraint(Observable<EntityTrait> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        entityName : entity.entityName.value.map{it.toString()}.orElse(null),
        traitName : entity.traitName.value.map{it.toString()}.orElse(null),
      ])
      EntityTrait another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<EntityTrait> duplicated = it.get('__duplicated__')
      new ConstraintViolation<EntityTrait>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'エンティティ名, トレイト名',
          'エンティティ-トレイト',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<EntityTrait>> validateCardinalityConstraint(Observable<EntityTrait> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.entity == null) {
        violations.add(new ConstraintViolation<EntityTrait>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'エンティティ',
            "名称=${entity.entityName}",
            entity,
          )
        ))
      }
      if (entity.trait == null) {
        violations.add(new ConstraintViolation<EntityTrait>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'トレイト',
            "名称=${entity.traitName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<EntityTrait>> validatePropertyType(Observable<EntityTrait> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.entityName.messageIfInvalid.map{['エンティティ名', it]}.orElse(null),
        entity.traitName.messageIfInvalid.map{['トレイト名', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<EntityTrait>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<EntityTrait>> validateRequiredProperty(Observable<EntityTrait> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.entityName.literal.map{''}.orElse('エンティティ名'),
        entity.traitName.literal.map{''}.orElse('トレイト名'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<EntityTrait>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}