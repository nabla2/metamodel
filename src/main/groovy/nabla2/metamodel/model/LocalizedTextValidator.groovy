package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 多言語化テキストバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class LocalizedTextValidator implements Validator<LocalizedText> {
  Observable<ConstraintViolation<LocalizedText>> validate(Observable<Table> table$) {
    Observable<LocalizedText> entity$ = LocalizedText.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<LocalizedText>> validateUniqueConstraint(Observable<LocalizedText> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        bundleName : entity.bundleName.value.map{it.toString()}.orElse(null),
        locale : entity.locale.value.map{it.toString()}.orElse(null),
        key : entity.key.value.map{it.toString()}.orElse(null),
      ])
      LocalizedText another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<LocalizedText> duplicated = it.get('__duplicated__')
      new ConstraintViolation<LocalizedText>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '区分名, ロケール, キー',
          '多言語化テキスト',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<LocalizedText>> validateCardinalityConstraint(Observable<LocalizedText> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.bundle == null) {
        violations.add(new ConstraintViolation<LocalizedText>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '多言語化テキスト区分',
            "名称=${entity.bundleName}, ロケール=${entity.locale}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<LocalizedText>> validatePropertyType(Observable<LocalizedText> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.bundleName.messageIfInvalid.map{['区分名', it]}.orElse(null),
        entity.locale.messageIfInvalid.map{['ロケール', it]}.orElse(null),
        entity.key.messageIfInvalid.map{['キー', it]}.orElse(null),
        entity.content.messageIfInvalid.map{['内容', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<LocalizedText>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<LocalizedText>> validateRequiredProperty(Observable<LocalizedText> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.bundleName.literal.map{''}.orElse('区分名'),
        entity.locale.literal.map{''}.orElse('ロケール'),
        entity.key.literal.map{''}.orElse('キー'),
        entity.content.literal.map{''}.orElse('内容'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<LocalizedText>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}