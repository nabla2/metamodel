package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.Entity
import nabla2.generator.model.GeneratorTask
/**
 * 領域
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Domain {

  /** 論理名 */
  static final String ENTITY_NAME = "領域"

  // ----- プロパティ定義 ------ //

  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 名前空間
   */
  JavaNamespace namespace

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * エンティティリスト
   */
  @Memoized
  @JsonIgnore
  List<Entity> getEntities() {
    Entity.from(_table$).filter {
      this.name.sameAs(it.domainName)
    }.toList().blockingGet()
  }
  /**
   * 自動生成タスクリスト
   */
  @Memoized
  @JsonIgnore
  List<GeneratorTask> getGeneratorTasks() {
    GeneratorTask.from(_table$).filter {
      this.name.sameAs(it.domainName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Domain> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Domain(
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            namespace : new JavaNamespace(row['名前空間']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  /**
   * 完全修飾名
   */
  @Memoized
  String getFqn() {
    namespace + (namespace.empty ? '' : '.') + className
  }

  /**
   * クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.orElse('')
  }

  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}