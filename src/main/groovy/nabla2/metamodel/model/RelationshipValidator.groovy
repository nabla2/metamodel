package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * リレーションバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class RelationshipValidator implements Validator<Relationship> {
  Observable<ConstraintViolation<Relationship>> validate(Observable<Table> table$) {
    Observable<Relationship> entity$ = Relationship.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Relationship>> validateUniqueConstraint(Observable<Relationship> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        entityName : entity.entityName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      Relationship another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<Relationship> duplicated = it.get('__duplicated__')
      new ConstraintViolation<Relationship>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'エンティティ名, 名称',
          'リレーション',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<Relationship>> validateCardinalityConstraint(Observable<Relationship> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.referenceEntity == null) {
        violations.add(new ConstraintViolation<Relationship>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'エンティティ',
            "名称=${entity.referenceEntityName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Relationship>> validatePropertyType(Observable<Relationship> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.entityName.messageIfInvalid.map{['エンティティ名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.referenceEntityName.messageIfInvalid.map{['参照エンティティ名', it]}.orElse(null),
        entity.cardinality.messageIfInvalid.map{['多重度', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Relationship>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Relationship>> validateRequiredProperty(Observable<Relationship> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.entityName.literal.map{''}.orElse('エンティティ名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.referenceEntityName.literal.map{''}.orElse('参照エンティティ名'),
        entity.cardinality.literal.map{''}.orElse('多重度'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Relationship>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}