package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
/**
 * 外部リソース
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class ExternalResource {

  /** 論理名 */
  static final String ENTITY_NAME = "外部リソース"

  // ----- プロパティ定義 ------ //

  /**
   * 名称
   */
  SingleLineText name
  /**
   * パス
   */
  SingleLineText path
  /**
   * ローカルパス
   */
  SingleLineText localPath
  /**
   * OS
   */
  SingleLineText os
  /**
   * アーキテクチャ
   */
  SingleLineText architecture

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<ExternalResource> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new ExternalResource(
            name : new SingleLineText(row['名称']),
            path : new SingleLineText(row['パス']),
            localPath : new SingleLineText(row['ローカルパス']),
            os : new SingleLineText(row['OS']),
            architecture : new SingleLineText(row['アーキテクチャ']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}