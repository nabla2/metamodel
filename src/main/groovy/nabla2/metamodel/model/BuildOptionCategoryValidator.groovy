package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * ビルドオプション区分バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class BuildOptionCategoryValidator implements Validator<BuildOptionCategory> {
  Observable<ConstraintViolation<BuildOptionCategory>> validate(Observable<Table> table$) {
    Observable<BuildOptionCategory> entity$ = BuildOptionCategory.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<BuildOptionCategory>> validateUniqueConstraint(Observable<BuildOptionCategory> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      BuildOptionCategory another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<BuildOptionCategory> duplicated = it.get('__duplicated__')
      new ConstraintViolation<BuildOptionCategory>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '名称',
          'ビルドオプション区分',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<BuildOptionCategory>> validateCardinalityConstraint(Observable<BuildOptionCategory> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<BuildOptionCategory>> validatePropertyType(Observable<BuildOptionCategory> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.value.messageIfInvalid.map{['說明', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<BuildOptionCategory>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<BuildOptionCategory>> validateRequiredProperty(Observable<BuildOptionCategory> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<BuildOptionCategory>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}