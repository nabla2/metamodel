package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.trait.PropertyTrait
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.DataType
/**
 * プロパティ
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Property implements PropertyTrait<Property> {

  /** 論理名 */
  static final String ENTITY_NAME = "プロパティ"

  // ----- プロパティ定義 ------ //

  /**
   * エンティティ名
   */
  SingleLineText entityName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 制約
   */
  SingleLineText constraint
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 型
   */
  SingleLineText typeName
  /**
   * デフォルト値
   */
  MultiLineText defaultValue

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * エンティティ
   */
  @Memoized
  @JsonIgnore
  Entity getEntity() {
    Entity.from(_table$).filter {
      this.entityName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 型
   */
  @Memoized
  @JsonIgnore
  DataType getType() {
    DataType.from(_table$).filter {
      this.typeName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Property> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Property(
            entityName : new SingleLineText(row['エンティティ名']),
            name : new SingleLineText(row['名称']),
            constraint : new SingleLineText(row['制約']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            typeName : new SingleLineText(row['型']),
            defaultValue : new MultiLineText(row['デフォルト値']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}