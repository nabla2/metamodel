package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.BooleanFlag
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.trait.LocalizedTextBundleTrait
import nabla2.metamodel.model.LocalizedText
/**
 * 多言語化テキスト区分
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class LocalizedTextBundle implements LocalizedTextBundleTrait<LocalizedTextBundle> {

  /** 論理名 */
  static final String ENTITY_NAME = "多言語化テキスト区分"

  // ----- プロパティ定義 ------ //

  /**
   * 名称
   */
  SingleLineText name
  /**
   * ロケール
   */
  SingleLineText locale
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * デフォルト
   */
  BooleanFlag isDefault
  /**
   * 説明
   */
  MultiLineText description

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 多言語化テキストリスト
   */
  @Memoized
  @JsonIgnore
  List<LocalizedText> getLocalizedTextList() {
    LocalizedText.from(_table$).filter {
      this.name.sameAs(it.bundleName) &&
      this.locale.sameAs(it.locale)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<LocalizedTextBundle> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new LocalizedTextBundle(
            name : new SingleLineText(row['名称']),
            locale : new SingleLineText(row['ロケール']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            isDefault : new BooleanFlag(row['デフォルト']),
            description : new MultiLineText(row['説明']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}