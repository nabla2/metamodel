package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * リレーション-プロパティバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class RelationshipPropertyValidator implements Validator<RelationshipProperty> {
  Observable<ConstraintViolation<RelationshipProperty>> validate(Observable<Table> table$) {
    Observable<RelationshipProperty> entity$ = RelationshipProperty.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<RelationshipProperty>> validateUniqueConstraint(Observable<RelationshipProperty> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        relationshipName : entity.relationshipName.value.map{it.toString()}.orElse(null),
        entityName : entity.entityName.value.map{it.toString()}.orElse(null),
        propertyName : entity.propertyName.value.map{it.toString()}.orElse(null),
        referenceEntityName : entity.referenceEntityName.value.map{it.toString()}.orElse(null),
        referencePropertyName : entity.referencePropertyName.value.map{it.toString()}.orElse(null),
      ])
      RelationshipProperty another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<RelationshipProperty> duplicated = it.get('__duplicated__')
      new ConstraintViolation<RelationshipProperty>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'リレーション名, エンティティ名, プロパティ名, 参照エンティティ名, 参照プロパティ名',
          'リレーション-プロパティ',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<RelationshipProperty>> validateCardinalityConstraint(Observable<RelationshipProperty> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.property == null) {
        violations.add(new ConstraintViolation<RelationshipProperty>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロパティ',
            "エンティティ名=${entity.entityName}, 名称=${entity.propertyName}",
            entity,
          )
        ))
      }
      if (entity.referenceProperty == null) {
        violations.add(new ConstraintViolation<RelationshipProperty>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロパティ',
            "エンティティ名=${entity.referenceEntityName}, 名称=${entity.referencePropertyName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<RelationshipProperty>> validatePropertyType(Observable<RelationshipProperty> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.relationshipName.messageIfInvalid.map{['リレーション名', it]}.orElse(null),
        entity.entityName.messageIfInvalid.map{['エンティティ名', it]}.orElse(null),
        entity.propertyName.messageIfInvalid.map{['プロパティ名', it]}.orElse(null),
        entity.referenceEntityName.messageIfInvalid.map{['参照エンティティ名', it]}.orElse(null),
        entity.referencePropertyName.messageIfInvalid.map{['参照プロパティ名', it]}.orElse(null),
        entity.nullValue.messageIfInvalid.map{['Null値', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<RelationshipProperty>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<RelationshipProperty>> validateRequiredProperty(Observable<RelationshipProperty> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.relationshipName.literal.map{''}.orElse('リレーション名'),
        entity.entityName.literal.map{''}.orElse('エンティティ名'),
        entity.propertyName.literal.map{''}.orElse('プロパティ名'),
        entity.referenceEntityName.literal.map{''}.orElse('参照エンティティ名'),
        entity.referencePropertyName.literal.map{''}.orElse('参照プロパティ名'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<RelationshipProperty>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}