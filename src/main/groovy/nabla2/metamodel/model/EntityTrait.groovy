package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Trait
/**
 * エンティティ-トレイト
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class EntityTrait {

  /** 論理名 */
  static final String ENTITY_NAME = "エンティティ-トレイト"

  // ----- プロパティ定義 ------ //

  /**
   * エンティティ名
   */
  SingleLineText entityName
  /**
   * トレイト名
   */
  SingleLineText traitName

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * エンティティ
   */
  @Memoized
  @JsonIgnore
  Entity getEntity() {
    Entity.from(_table$).filter {
      this.entityName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * トレイト
   */
  @Memoized
  @JsonIgnore
  Trait getTrait() {
    Trait.from(_table$).filter {
      this.traitName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<EntityTrait> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new EntityTrait(
            entityName : new SingleLineText(row['エンティティ名']),
            traitName : new SingleLineText(row['トレイト名']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}