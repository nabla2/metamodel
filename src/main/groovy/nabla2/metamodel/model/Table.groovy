package nabla2.metamodel.model

import groovy.transform.Immutable
import io.reactivex.Observable
import io.reactivex.functions.Predicate

@Immutable(copyWith = true)
class Table {
  static final String LINE_COMMENT_SYMBOL = '##'
  Cell name
  Row header
  List<DefaultCellValue> metadata
  Cell pivotAxis
  List<Row> rows
  Map<String, Map<String,String>> globalMetadata = [:]

  Map<String, String> getMetadataAsMap() {

    Map<String, String> result = [:]
    result.putAll(globalMetadata.getOrDefault('*', [:]))
    result.putAll(globalMetadata.getOrDefault(nameAsText, [:]))
    metadata.each {
      result[it.name] = it.value
    }
    result
  }

  List<Map<String, String>> getRowsAsMap() {
    if (!this.pivotAxis) {
      return rows.collect { Row row ->
        Map<String, String> record = metadataAsMap
        header.cells.each { head ->
          record[head.content] = row.cells.find { it.colIndex == head.colIndex }?.content
        }
        record
      }
    }
    List<String> tokens = pivotAxis.content.replaceAll(/\=\>\s*$/, '').tokenize('/').collect{it.trim()}
    String valueColName = tokens.first()
    String axisColName = tokens.last()
    List<Cell> colHeader = header.cells.findAll{ it.colIndex < pivotAxis.colIndex }
    List<Cell> pivotHeader = header.cells.findAll{ it.colIndex >= pivotAxis.colIndex }
    return rows.collectMany { Row row ->
      pivotHeader.collect { pivotHead ->
        Map<String, String> record = metadataAsMap
        colHeader.each { head ->
          record[head.content] = row.cells.find { it.colIndex == head.colIndex }?.content
        }
        record[axisColName] = pivotHead?.content
        record[valueColName] = row.cells.find{ it.colIndex == pivotHead.colIndex}?.content
        record
      }
    }
  }

  Table filter(Predicate<Row> predicate) {
    this.copyWith(
      rows: rows.findAll{ predicate.test(it) }
    )
  }

  String getRange() {
    if (rows.empty) return "NaN:NaN"
    String from = rows.first().cells.with{it.empty ? 'NaN' : it.first().name}
    String to = rows.last().cells.with{it.empty ? 'NaN' : it.last().name}
    "${from}:${to}"
  }

  String toString() {
  """\
  |[${name.book.path.tokenize('/').last()}](${name.book.normalize()}) ${name.pageName}!${range}
  |
  ||   /   | ${header.cells.collect{it.colName.padLeft(3) + '  '}.join(' | ')} |
  || ----- | ${header.cells.collect{'-----'}.join(' | ')} |
  || ${rowNum(name)} | ${name.content} |
  ${metadata.collect{"""\
  || ${rowNum(it.row)} | ${it.name}: | ${it.value} |
  """}.join('').trim()}
  ${header.cells.empty ? '' : """\
  || ${rowNum(header)} | ${header.cells.collect{ it.content }.join(' | ')} |
  ${rows.collect {row -> """\
  || ${rowNum(row)} | ${row.cells.collect { it.content }.join(' | ') } |
  """}.join('').trim()}
  """.trim()}
  |
  |￣￣￣￣￣|${name.pageName}|￣￣￣￣￣
  |
  """.stripMargin()
  }

  static String rowNum(Cell c) {
    String.valueOf(c.rowIndex + 1).padLeft(5)
  }

  static String rowNum(Row r) {
    rowNum(r.cells.first())
  }

  static Observable<Table> from(Observable<Cell> cell$) {
    tablesFrom(rowsFrom(cell$))
  }

  static Observable<Table> tablesFrom(Observable<Row> row$) {
    row$
    .map {[it]}
    .scan(new ArrayList<Row>()) { acc, next ->
      (acc.empty || acc.last().lastOfTable) ? next
                                            : acc + next
    }
    .filter { rows ->
      !rows.empty && rows.last().isLastOfTable()
    }
    .map {
      it.findAll{it.cells.first().content != LINE_COMMENT_SYMBOL}
    }
    .filter {
      it.size() > 0
    }
    .map { List<Row> rows ->
      if (rows.size() <= 1) throw new IllegalStateException(
        "The following table does not have any content:\n${rows[0].toString()}"
      )
      rows[1..-1]
      .split { it.cells[0].content.endsWith(':') }
      .with {
        List<Row> metadata = it[0]
        Cell pivotAxis = (it[1].size() <= 1) ? null
                       : (it[1][0].cells.with {
                            it && it[0].content.endsWith('=>') ? it[0] : null
                         })
        List<Row> tableBody = pivotAxis ? it[1][1..-1] : it[1]
        if (metadata.empty && tableBody.empty) throw new IllegalStateException(
          "The following table does not have any content:\n${rows[0].toString()}"
        )
        new Table(
          name      :rows.first().cells[0],
          metadata  :metadata.collect{ new DefaultCellValue(it) },
          pivotAxis :pivotAxis,
          header    :tableBody.empty ? new Row(cells:[], rowIndex:0, lastOfTable: true) : tableBody[0],
          rows      :tableBody.empty ? [new Row(cells:[], rowIndex:0, lastOfTable: true)] : tableBody.subList(1, tableBody.size()),
        )
      }
    }
  }

  static Observable<Row> rowsFrom(Observable<Cell> cell$) {
    cell$
    .map{ [it] }
    .scan(new ArrayList<Cell>()) { acc, next ->
      (acc.empty || acc.last().lastOfRow) ? next : (acc + next)
    }
    .filter {
      !it.empty && it.last().lastOfRow
    }
    .map {
      new Row(
        cells       :it,
        rowIndex    :it.first().rowIndex,
        lastOfTable :it.first().lastOfTable,
      )
    }

  }

  String getNameAsText() {
    name.content
        .replace(OPEN_BRACE_MARK, '')
        .replace(CLOSE_BRACE_MARK, '')
  }

  static final String OPEN_BRACE_MARK = '【'
  static final String CLOSE_BRACE_MARK = '】'
}
