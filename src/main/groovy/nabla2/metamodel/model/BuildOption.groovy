package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.BuildOptionCategory
/**
 * ビルドオプション
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class BuildOption {

  /** 論理名 */
  static final String ENTITY_NAME = "ビルドオプション"

  // ----- プロパティ定義 ------ //

  /**
   * 環境
   */
  SingleLineText environment
  /**
   * 区分名
   */
  SingleLineText categoryName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 値
   */
  SingleLineText value
  /**
   * コメント
   */
  SingleLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 区分
   */
  @Memoized
  @JsonIgnore
  BuildOptionCategory getCategory() {
    BuildOptionCategory.from(_table$).filter {
      this.categoryName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<BuildOption> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new BuildOption(
            environment : new SingleLineText(row['環境'] ?: "ANY"),
            categoryName : new SingleLineText(row['区分名']),
            name : new SingleLineText(row['名称']),
            value : new SingleLineText(row['値']),
            comment : new SingleLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}