package nabla2.metamodel.model

import groovy.transform.Immutable
import org.apache.poi.hssf.util.CellReference

@Immutable(copyWith = true)
class Cell {
  String content
  int colIndex
  int rowIndex
  String pageName
  URI book
  boolean lastOfRow
  boolean lastOfTable

  boolean isInSameRowWith(Cell another) {
    if (!another) return false
    (this.book     == another.book)     &&
    (this.pageName == another.pageName) &&
    (this.rowIndex == another.rowIndex)
  }

  String getColName() {
    CellReference.convertNumToColString(colIndex)
  }

  String getName() {
    "${colName}${rowIndex+1}"
  }

  @Override
  String toString() {
    "$content($rowIndex${lastOfTable ? '!' : ''})"
  }
}
