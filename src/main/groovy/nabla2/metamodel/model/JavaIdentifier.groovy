package nabla2.metamodel.model

import groovy.transform.Memoized

trait JavaIdentifier {

  static String normalize(String name) {
     name
     .replaceAll(/[^_0-9a-zA-Z$\s]+/, '')
  }

  static String upperCamelize(String name) {
    name
    .split(/[_\s]+/)
    .collect { it[0].toUpperCase() + it.substring(1) }
    .join('')
  }

  static String lowerCamelize(String name) {
    upperCamelize(name).with {
      it[0].toLowerCase() + it.substring(1)
    }
  }

  abstract String getIdentifier()

  @Memoized
  String getUpperCamerizedName() {
    upperCamelize(identifier)
  }

  @Memoized
  String getLowerCamerizedName() {
    lowerCamelize(identifier)
  }

  @Memoized
  String getClassName() {
    upperCamerizedName
  }

  @Memoized
  String getClassMemberName() {
    lowerCamerizedName
  }
}
