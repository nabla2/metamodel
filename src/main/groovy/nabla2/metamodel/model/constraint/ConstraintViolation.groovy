package nabla2.metamodel.model.constraint

import groovy.transform.Canonical

@Canonical
class ConstraintViolation<T> {
  String message
  List<T> causedBy
  String toString() {
    "### ${message}"
  }
}
