package nabla2.metamodel.model.constraint

import io.reactivex.Observable
import nabla2.metamodel.model.Table

interface Validator<T> {
  Observable<ConstraintViolation<T>> validate(Observable<Table> table$)
}