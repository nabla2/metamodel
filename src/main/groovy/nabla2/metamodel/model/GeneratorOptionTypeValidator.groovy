package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 自動生成オプション種別バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class GeneratorOptionTypeValidator implements Validator<GeneratorOptionType> {
  Observable<ConstraintViolation<GeneratorOptionType>> validate(Observable<Table> table$) {
    Observable<GeneratorOptionType> entity$ = GeneratorOptionType.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<GeneratorOptionType>> validateUniqueConstraint(Observable<GeneratorOptionType> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      GeneratorOptionType another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<GeneratorOptionType> duplicated = it.get('__duplicated__')
      new ConstraintViolation<GeneratorOptionType>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '名称',
          '自動生成オプション種別',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<GeneratorOptionType>> validateCardinalityConstraint(Observable<GeneratorOptionType> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.targetTask == null) {
        violations.add(new ConstraintViolation<GeneratorOptionType>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '自動生成タスク',
            "名称=${entity.targetTaskName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<GeneratorOptionType>> validatePropertyType(Observable<GeneratorOptionType> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.targetTaskName.messageIfInvalid.map{['対象タスク名', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.value.messageIfInvalid.map{['說明', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<GeneratorOptionType>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<GeneratorOptionType>> validateRequiredProperty(Observable<GeneratorOptionType> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.literal.map{''}.orElse('名称'),
        entity.targetTaskName.literal.map{''}.orElse('対象タスク名'),
        entity.identifier.literal.map{''}.orElse('識別子'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<GeneratorOptionType>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}