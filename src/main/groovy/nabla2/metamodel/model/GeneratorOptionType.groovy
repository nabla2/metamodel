package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.generator.model.GeneratorTask
import nabla2.metamodel.model.GeneratorOption
/**
 * 自動生成オプション種別
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class GeneratorOptionType {

  /** 論理名 */
  static final String ENTITY_NAME = "自動生成オプション種別"

  // ----- プロパティ定義 ------ //

  /**
   * 名称
   */
  SingleLineText name
  /**
   * 対象タスク名
   */
  SingleLineText targetTaskName
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 說明
   */
  MultiLineText value

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 対象タスク
   */
  @Memoized
  @JsonIgnore
  GeneratorTask getTargetTask() {
    GeneratorTask.from(_table$).filter {
      this.targetTaskName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * オプションリスト
   */
  @Memoized
  @JsonIgnore
  List<GeneratorOption> getOptions() {
    GeneratorOption.from(_table$).filter {
      this.name.sameAs(it.optionTypeName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<GeneratorOptionType> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new GeneratorOptionType(
            name : new SingleLineText(row['名称']),
            targetTaskName : new SingleLineText(row['対象タスク名']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            value : new MultiLineText(row['說明']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}