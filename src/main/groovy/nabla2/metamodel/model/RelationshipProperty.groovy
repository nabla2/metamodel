package nabla2.metamodel.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.Property
import nabla2.metamodel.model.Property
/**
 * リレーション-プロパティ
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class RelationshipProperty {

  /** 論理名 */
  static final String ENTITY_NAME = "リレーション-プロパティ"

  // ----- プロパティ定義 ------ //

  /**
   * リレーション名
   */
  SingleLineText relationshipName
  /**
   * エンティティ名
   */
  SingleLineText entityName
  /**
   * プロパティ名
   */
  SingleLineText propertyName
  /**
   * 参照エンティティ名
   */
  SingleLineText referenceEntityName
  /**
   * 参照プロパティ名
   */
  SingleLineText referencePropertyName
  /**
   * Null値
   */
  SingleLineText nullValue

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * プロパティ
   */
  @Memoized
  @JsonIgnore
  Property getProperty() {
    Property.from(_table$).filter {
      this.entityName.sameAs(it.entityName) &&
      this.propertyName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 参照プロパティ
   */
  @Memoized
  @JsonIgnore
  Property getReferenceProperty() {
    Property.from(_table$).filter {
      this.referenceEntityName.sameAs(it.entityName) &&
      this.referencePropertyName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<RelationshipProperty> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new RelationshipProperty(
            relationshipName : new SingleLineText(row['リレーション名']),
            entityName : new SingleLineText(row['エンティティ名']),
            propertyName : new SingleLineText(row['プロパティ名']),
            referenceEntityName : new SingleLineText(row['参照エンティティ名']),
            referencePropertyName : new SingleLineText(row['参照プロパティ名']),
            nullValue : new SingleLineText(row['Null値']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}