package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * ビルドオプションバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class BuildOptionValidator implements Validator<BuildOption> {
  Observable<ConstraintViolation<BuildOption>> validate(Observable<Table> table$) {
    Observable<BuildOption> entity$ = BuildOption.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<BuildOption>> validateUniqueConstraint(Observable<BuildOption> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        environment : entity.environment.value.map{it.toString()}.orElse(null),
        categoryName : entity.categoryName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      BuildOption another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<BuildOption> duplicated = it.get('__duplicated__')
      new ConstraintViolation<BuildOption>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '環境, 区分名, 名称',
          'ビルドオプション',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<BuildOption>> validateCardinalityConstraint(Observable<BuildOption> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.category == null) {
        violations.add(new ConstraintViolation<BuildOption>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'ビルドオプション区分',
            "名称=${entity.categoryName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<BuildOption>> validatePropertyType(Observable<BuildOption> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.environment.messageIfInvalid.map{['環境', it]}.orElse(null),
        entity.categoryName.messageIfInvalid.map{['区分名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.value.messageIfInvalid.map{['値', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<BuildOption>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<BuildOption>> validateRequiredProperty(Observable<BuildOption> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.environment.literal.map{''}.orElse('環境'),
        entity.categoryName.literal.map{''}.orElse('区分名'),
        entity.name.literal.map{''}.orElse('名称'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<BuildOption>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}