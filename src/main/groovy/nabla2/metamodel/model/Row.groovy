package nabla2.metamodel.model

import groovy.transform.Immutable
import groovy.transform.Memoized
import org.gradle.internal.impldep.com.fasterxml.jackson.annotation.JsonIgnore

@Immutable(copyWith = true)
class Row {
  @JsonIgnore
  List<Cell> cells
  int rowIndex
  boolean lastOfTable

  @Memoized
  String getRange() {
    "${cells.first().name}:${cells.last().name}"
  }

  @Memoized
  String getPageName() {
    cells.first().pageName
  }

  @Override
  String toString() {
  """\
  |${range}@${cells.first().book.path}
  |${cells.collect{it.content}.join(' | ')}
  """.stripMargin().trim()
  }
}
