package nabla2.metamodel.model

import groovy.transform.Canonical
import groovy.transform.Memoized

@Canonical
class DefaultCellValue {
  Row row

  @Memoized
  String getName() {
    row.cells[0].content.replaceAll(/:$/, '')
  }

  String getValue() {
    if (!row.cells[1]) throw new IllegalStateException(
      "Illegal default value in a row:\n${row}"
    )
    row.cells[1].content
  }

}
