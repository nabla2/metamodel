package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * プロパティバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class PropertyValidator implements Validator<Property> {
  Observable<ConstraintViolation<Property>> validate(Observable<Table> table$) {
    Observable<Property> entity$ = Property.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Property>> validateUniqueConstraint(Observable<Property> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        entityName : entity.entityName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      Property another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<Property> duplicated = it.get('__duplicated__')
      new ConstraintViolation<Property>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'エンティティ名, 名称',
          'プロパティ',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<Property>> validateCardinalityConstraint(Observable<Property> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.entity == null) {
        violations.add(new ConstraintViolation<Property>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'エンティティ',
            "名称=${entity.entityName}",
            entity,
          )
        ))
      }
      if (entity.type == null) {
        violations.add(new ConstraintViolation<Property>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'データ型',
            "名称=${entity.typeName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Property>> validatePropertyType(Observable<Property> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.entityName.messageIfInvalid.map{['エンティティ名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.constraint.messageIfInvalid.map{['制約', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.typeName.messageIfInvalid.map{['型', it]}.orElse(null),
        entity.defaultValue.messageIfInvalid.map{['デフォルト値', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Property>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Property>> validateRequiredProperty(Observable<Property> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.entityName.literal.map{''}.orElse('エンティティ名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.constraint.literal.map{''}.orElse('制約'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.typeName.literal.map{''}.orElse('型'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Property>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}