package nabla2.metamodel.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 多言語化テキスト区分バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class LocalizedTextBundleValidator implements Validator<LocalizedTextBundle> {
  Observable<ConstraintViolation<LocalizedTextBundle>> validate(Observable<Table> table$) {
    Observable<LocalizedTextBundle> entity$ = LocalizedTextBundle.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<LocalizedTextBundle>> validateUniqueConstraint(Observable<LocalizedTextBundle> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        name : entity.name.value.map{it.toString()}.orElse(null),
        locale : entity.locale.value.map{it.toString()}.orElse(null),
      ])
      LocalizedTextBundle another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<LocalizedTextBundle> duplicated = it.get('__duplicated__')
      new ConstraintViolation<LocalizedTextBundle>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '名称, ロケール',
          '多言語化テキスト区分',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<LocalizedTextBundle>> validateCardinalityConstraint(Observable<LocalizedTextBundle> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<LocalizedTextBundle>> validatePropertyType(Observable<LocalizedTextBundle> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.locale.messageIfInvalid.map{['ロケール', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.isDefault.messageIfInvalid.map{['デフォルト', it]}.orElse(null),
        entity.description.messageIfInvalid.map{['説明', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<LocalizedTextBundle>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<LocalizedTextBundle>> validateRequiredProperty(Observable<LocalizedTextBundle> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.literal.map{''}.orElse('名称'),
        entity.locale.literal.map{''}.orElse('ロケール'),
        entity.identifier.literal.map{''}.orElse('識別子'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<LocalizedTextBundle>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}