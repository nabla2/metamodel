package nabla2.generator.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.gradle.BasePlugin
import nabla2.metamodel.model.Domain

@Canonical
class GeneratorGradlePluginGroovy implements TextFileTemplate {

  Domain feature

  @Memoized
  String getSource() {
  """\
  |package ${feature.namespace}.gradle
  |
  |import org.gradle.api.Project
  |import ${BasePlugin.canonicalName}
  ${feature.generatorTasks.collect{ """\
  |import ${it.fqn}
  """.trim()}.join('\n')}
  |
  |/**
  | * ${feature.name}自動生成Gradleプラグイン
  | *
  | * ${regenerationMark}
  | */
  |class ${feature.className}GradlePlugin extends ${BasePlugin.simpleName} {
  |
  |  void apply(Project project) {
  ${feature.generatorTasks.collect{ """\
  |    register(
  |      project,
  |      ${it.className},
  |      ${it.className}.Settings,
  |    )
  """.trim()}.join('\n')}
  |  }
  |}
  """.trim().stripMargin()
  }

  @Override
  String getRelPath() {
    String fqn = "${feature.namespace}.gradle.${feature.className}GradlePlugin"
    fqn.replace('.', '/') + '.groovy'
  }
}
