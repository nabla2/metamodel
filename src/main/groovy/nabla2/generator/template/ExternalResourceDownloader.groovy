package nabla2.generator.template

import groovy.transform.Canonical
import nabla2.generator.FileTemplate
import nabla2.metamodel.model.ExternalResource
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Canonical
class ExternalResourceDownloader implements FileTemplate {

  ExternalResource resource

  String regenerationMark

  @Override
  boolean needsRegenerated(File file) {
    if (file.exists()) return false
    String osName = System.getProperty('os.name').toLowerCase()
    String osArch = System.getProperty('os.arch').toLowerCase()
    resource.os.value.map{osName.contains(it.toLowerCase())}
            .orElse(true) &&
    resource.architecture.value.map{osArch.contains(it.toLowerCase())}
            .orElse(true)
  }

  @Override
  String getRelPath() {
    resource.localPath
  }

  @Override
  InputStream getResourceAsStream() {
    println "Downloading... ${resource.name} (${resource.path})\n  ==> ${resource.localPath}"
    new URL(resource.path.get()).openStream()
  }

  Logger logger = LoggerFactory.getLogger(ExternalResourceDownloader)
}
