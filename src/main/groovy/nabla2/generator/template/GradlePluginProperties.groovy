package nabla2.generator.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.Domain

@Canonical
class GradlePluginProperties implements TextFileTemplate {

  Domain feature

  String getPluginClassFqn() {
    "${feature.namespace}.gradle.${feature.className}GradlePlugin"
  }

  @Memoized
  String getSource() {

  """\
  |# ${feature.name} Gradleプラグイン定義
  |# ${regenerationMark}
  |implementation-class=${pluginClassFqn}
  """.trim().stripMargin()
  }

  @Override
  String getRelPath() {
    return "META-INF/gradle-plugins/${feature.namespace}.${feature.identifier}.properties"
  }
}
