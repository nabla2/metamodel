package nabla2.generator.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.generator.model.GeneratorTask

@Canonical
class GeneratorTaskOptionSetGroovy implements TextFileTemplate {

  GeneratorTask task

  @Memoized
  String getSource() {
    """\
    |package ${task.namespace}
    |
    |import nabla2.metamodel.model.GeneratorOption
    |import groovy.transform.Canonical
    |import groovy.transform.Memoized
    |
    |/**
    | * ${task.name}オプション定義
    | *
    | * ${regenerationMark}
    | */
    |@Canonical
    |class ${task.className}OptionSet {
    |
    |  List<GeneratorOption> allOptions
    |  Map<String, ?> properties
    |
    |  /**
    |   * 環境フラグを表すビルドプロパティを取得する。
    |   */
    |  String getEnv() {
    |    properties.find{it.key.toLowerCase() == 'env'}.with {
    |      (it == null || it.value.toString().trim().empty) ? '' : it.value.toString().toUpperCase()
    |    }
    |  }
    |
    ${task.generatorOptionTypes.collect{ opts -> """\
    |  /**
    |   * ${opts.name}
    |   */
    |  @Memoized
    |  Map<String, String> get${opts.identifier.upperCamelized.get()}() {
    |    Map<String, String> result = [:]
    |    def defaults = allOptions.findAll{ !it.value.empty && it.environment.value.map{it.toUpperCase() == 'ANY'}.orElse(false) }
    |    def forCurrentEnv = allOptions.findAll{ !it.value.empty && it.environment.value.map{it.toUpperCase() == env}.orElse(false) }
    |    defaults.forEach{ result[it.name.get()] = it.value.value.orElse('') }
    |    forCurrentEnv.forEach{ result[it.name.get()] = it.value.value.orElse('') }
    |    result
    |  }
    """.trim()}.join('\n')}
    |}
    """.stripMargin().trim().replaceAll(/\n\s*\n/, '\n\n')
  }

  @Override
  String getRelPath() {
    task.namespace.value.orElse('').replace('.', '/') + '/' + task.className + 'OptionSet.groovy'
  }

  @Override
  boolean needsRegenerated(File file) {
    if (task.generatorOptionTypes.empty) return false
    TextFileTemplate.super.needsRegenerated(file)
  }
}
