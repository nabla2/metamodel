package nabla2.generator.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.excel.ExcelWorkbookLoader
import nabla2.generator.TextFileTemplate
import nabla2.generator.model.GeneratorTask
import nabla2.generator.model.GeneratorTemplate
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.Table

@Canonical
class GeneratorGradleTaskGroovy implements TextFileTemplate {

  GeneratorTask task

  @Memoized
  String getSource() {
  """\
  |package ${task.namespace}
  |
  ${dependentModules.collect{"""\
  |import ${it}
  """}.join('').trim()}
  |/**
  | * ${task.name}
  | *
  | * ${regenerationMark}
  | */
  |class ${task.className} extends DefaultTask {
  |
  |  @InputFiles FileCollection workbooks
  |  @InputFiles @Optional FileCollection referringTo
  ${templatesWithTarget.collect{ template -> """\
  |  @OutputDirectory @Optional File ${template.identifier.lowerCamelized.get()}Dir
  """.trim()}.join('\n')}
  |  File baseDir
  |  @OutputFile @Optional File errorReport
  |  String group = 'laplacian generator'
  |  String description = '${task.comment.value.orElse(task.name.get())}'
  |
  |  @TaskAction
  |  void exec() {
  |    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
  |      ExcelWorkbookLoader.from(workbook).table\$
  |    }.with { _table\$ ->
  |      errorReport.text = ''
  |      Throwable error = null
  |      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table\$).toList().blockingGet().inject([:]) { acc, prop ->
  |        Map<String,String> bucket = acc[prop.entityName.get()]
  |        if (prop.value.empty) return acc
  |        if (!bucket) {
  |          bucket = [:]
  |          acc[prop.entityName.get()] = bucket
  |        }
  |        bucket[prop.propertyName.get()] = prop.value.get()
  |        acc
  |      }
  |      def table\$ = _table\$.map{ table ->
  |        table.copyWith(globalMetadata: commonProps)
  |      }
  |      validate(table\$)
  |      List<GeneratorOption> optionList = GeneratorOption.from(table\$).filter{
  |        it.optionType.targetTask.identifier.sameAs('${task.identifier.get()}')
  |      }.toList().blockingGet()
  ${task.templateList.collect { template ->
  """\
  |      ${template.generateFrom.className}.from(table\$)
  |      .filter {
  |        referringTo.every{book -> book.path != it._row.cells.first().book.path}
  |      }
  ${template.generationMaxCount.empty ? '' : """\
  |      .take(${template.generationMaxCount.get()})
  """.trim()}
  |      .doOnNext {
  ${task.generatorOptionTypes.empty ? """\
  |        new ${template.className}(it).with { template ->
  """.trim() : """\
  |        def opts = new ${task.className}OptionSet(optionList, project.properties)
  |        new ${template.className}(it).with { template ->
  |          template.metaClass.properties.find{it.type == ${task.className}OptionSet}.with { p ->
  |            if (p) p.setProperty(template, opts)
  |          }
  """.trim()}
  |          template.regenerationMark = "${template.regenerationMark}"
  ${(template.targetPath.get().startsWith('~/') ? """\
  |          template.generateTo(new File(System.getProperty("user.home"), '${template.targetPath.get().replaceFirst(/^~/, '')}'))
  """ : template.targetPath.get().startsWith('/') ?  """\
  |          template.generateTo(new File('${template.targetPath}'))
  """ : """\
  |          template.generateTo(new File(baseDir, '${template.targetPath}'))
  """).trim()}
  |        }
  |      }
  |      .doOnError{it.printStackTrace(); error = it}
  |      .onErrorResumeNext(Observable.empty())
  |      .toList()
  |      .blockingGet().with {
  |        if (error) throw new GradleException(
  |          "An error occurred while processing the model files."
  |        )
  |      }
  """}.join('').trim()}
  |    }
  |  }
  |
  |  void validate(Observable<Table> table\$) {
  |    Throwable error
  |    println 'Checking model files...'
  |    Observable.fromArray(
  ${task.feature.entities.collect{"""\
  |      new ${it.className}Validator().validate(table\$),
  """.trim()}.join('\n')}
  |    )
  |    .flatMap {it}
  |    .doOnNext{ println it; errorReport << it }
  |    .doOnError{ it.printStackTrace(); error = it }
  |    .onErrorResumeNext(Observable.empty())
  |    .toList()
  |    .blockingGet().with {
  |      if (!it.empty) throw new GradleException(
  |        "There are \${it.size()} errors in model files. see details in \${this.errorReport.path}"
  |      )
  |      if (error) throw new GradleException(
  |        "An error occurred while validating the model files.", error
  |      )
  |    }
  |  }
  |
  |  static class Settings extends TaskSettings<${task.className}> {
  |    FileCollection workbooks
  |    FileCollection referringTo
  |    File baseDir
  |    File errorReport
  |    void setWorkbook(File workbook) {
  |      workbooks = project.files(workbook)
  |    }
  |    @Override
  |    void setup(${task.className} task) {
  |      File baseDir = this.baseDir ?: new File('./')
  |      task.workbooks = workbooks
  |      task.referringTo = referringTo ?: project.files([])
  |      task.baseDir = baseDir
  |      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
  ${templatesWithTarget.collect{ template -> """\
  |      task.${template.identifier.lowerCamelized.get()}Dir = new File(baseDir, '${template.targetPath}')
  """.trim()}.join('\n')}
  |    }
  |  }
  |}
  """.trim().replaceAll(/\n\s*\n/, '\n').stripMargin()
  }

  List<GeneratorTemplate> getTemplatesWithTarget() {
    task.templateList.findAll{!it.targetPath.empty && !it.targetPath.sameAs('./')}
  }

  List<String> getDependentModules() {
    (['org.gradle.api.DefaultTask',
      'org.gradle.api.tasks.InputFiles',
      'org.gradle.api.tasks.OutputDirectory',
      'org.gradle.api.tasks.OutputFile',
      'org.gradle.api.tasks.Optional',
      'org.gradle.api.tasks.TaskAction',
      'org.gradle.api.file.FileCollection',
      'org.gradle.api.GradleException',
      'io.reactivex.Observable',
      GeneratorOption.class.canonicalName,
      Table.class.canonicalName,
      TaskSettings.class.canonicalName,
      ExcelWorkbookLoader.canonicalName,
      ModelCommonProperty.class.canonicalName,
    ] + task.templateList.collect{ it.fqn }
      + task.templateList.collect{ it.generateFrom.fqn }
      + task.feature.entities.collect{ it.fqn + "Validator"}
    ).sort().unique()
  }

  @Override
  String getRelPath() {
    task.namespace.value.orElse('').replace('.', '/') + '/' + task.className + '.groovy'
  }
}
