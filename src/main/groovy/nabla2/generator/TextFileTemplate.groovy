package nabla2.generator

trait TextFileTemplate implements FileTemplate {

  abstract String getSource()

  @Override
  InputStream getResourceAsStream() {
    new ByteArrayInputStream(source.getBytes('UTF-8'))
  }

  @Override
  boolean needsRegenerated(File file)  {
    if (!file.exists()) return true
    file.text.with {
      if (it == null || it.trim().empty) return true
      it.contains(regenerationMark)
    }
  }

  String getRegenerationMark() {
    regenerationMark ?: ''
  }

  void setRegenerationMark(String mark) {
    this.regenerationMark = mark
  }

  private regenerationMark = null
}