package nabla2.generator.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 自動生成テンプレートバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class GeneratorTemplateValidator implements Validator<GeneratorTemplate> {
  Observable<ConstraintViolation<GeneratorTemplate>> validate(Observable<Table> table$) {
    Observable<GeneratorTemplate> entity$ = GeneratorTemplate.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<GeneratorTemplate>> validateUniqueConstraint(Observable<GeneratorTemplate> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        taskName : entity.taskName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      GeneratorTemplate another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<GeneratorTemplate> duplicated = it.get('__duplicated__')
      new ConstraintViolation<GeneratorTemplate>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'タスク名, 名称',
          '自動生成テンプレート',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<GeneratorTemplate>> validateCardinalityConstraint(Observable<GeneratorTemplate> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.generateFrom == null) {
        violations.add(new ConstraintViolation<GeneratorTemplate>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'エンティティ',
            "名称=${entity.generateFromEntityName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<GeneratorTemplate>> validatePropertyType(Observable<GeneratorTemplate> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.taskName.messageIfInvalid.map{['タスク名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.generateFromEntityName.messageIfInvalid.map{['生成元エンティティ名', it]}.orElse(null),
        entity.namespace.messageIfInvalid.map{['名前空間', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.generationMaxCount.messageIfInvalid.map{['最大生成数', it]}.orElse(null),
        entity.targetPath.messageIfInvalid.map{['出力先パス', it]}.orElse(null),
        entity.targetFilePattern.messageIfInvalid.map{['生成対象ファイルパターン', it]}.orElse(null),
        entity.regenerationMark.messageIfInvalid.map{['再生成対象マーカ', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<GeneratorTemplate>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<GeneratorTemplate>> validateRequiredProperty(Observable<GeneratorTemplate> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.taskName.literal.map{''}.orElse('タスク名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.generateFromEntityName.literal.map{''}.orElse('生成元エンティティ名'),
        entity.namespace.literal.map{''}.orElse('名前空間'),
        entity.identifier.literal.map{''}.orElse('識別子'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<GeneratorTemplate>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}