package nabla2.generator.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.generator.model.GeneratorTemplate
import nabla2.metamodel.model.Domain
import nabla2.metamodel.model.GeneratorOptionType
/**
 * 自動生成タスク
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class GeneratorTask {

  /** 論理名 */
  static final String ENTITY_NAME = "自動生成タスク"

  // ----- プロパティ定義 ------ //

  /**
   * 領域名
   */
  SingleLineText domainName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 名前空間
   */
  JavaNamespace namespace
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * コメント
   */
  SingleLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * テンプレートリスト
   */
  @Memoized
  @JsonIgnore
  List<GeneratorTemplate> getTemplateList() {
    GeneratorTemplate.from(_table$).filter {
      this.name.sameAs(it.taskName)
    }.toList().blockingGet()
  }
  /**
   * 領域
   */
  @Memoized
  @JsonIgnore
  Domain getFeature() {
    Domain.from(_table$).filter {
      this.domainName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 自動生成オプション種別リスト
   */
  @Memoized
  @JsonIgnore
  List<GeneratorOptionType> getGeneratorOptionTypes() {
    GeneratorOptionType.from(_table$).filter {
      this.name.sameAs(it.targetTaskName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<GeneratorTask> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new GeneratorTask(
            domainName : new SingleLineText(row['領域名']),
            name : new SingleLineText(row['名称']),
            namespace : new JavaNamespace(row['名前空間']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            comment : new SingleLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  /**
   * 完全修飾名
   */
  @Memoized
  String getFqn() {
    namespace + (namespace.empty ? '' : '.') + className
  }

  /**
   * クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.orElse('')
  }

  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}