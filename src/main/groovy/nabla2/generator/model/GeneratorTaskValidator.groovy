package nabla2.generator.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 自動生成タスクバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class GeneratorTaskValidator implements Validator<GeneratorTask> {
  Observable<ConstraintViolation<GeneratorTask>> validate(Observable<Table> table$) {
    Observable<GeneratorTask> entity$ = GeneratorTask.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<GeneratorTask>> validateUniqueConstraint(Observable<GeneratorTask> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        domainName : entity.domainName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      GeneratorTask another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<GeneratorTask> duplicated = it.get('__duplicated__')
      new ConstraintViolation<GeneratorTask>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '領域名, 名称',
          '自動生成タスク',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<GeneratorTask>> validateCardinalityConstraint(Observable<GeneratorTask> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.feature == null) {
        violations.add(new ConstraintViolation<GeneratorTask>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '領域',
            "名称=${entity.domainName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<GeneratorTask>> validatePropertyType(Observable<GeneratorTask> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.domainName.messageIfInvalid.map{['領域名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.namespace.messageIfInvalid.map{['名前空間', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<GeneratorTask>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<GeneratorTask>> validateRequiredProperty(Observable<GeneratorTask> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.domainName.literal.map{''}.orElse('領域名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.namespace.literal.map{''}.orElse('名前空間'),
        entity.identifier.literal.map{''}.orElse('識別子'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<GeneratorTask>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}