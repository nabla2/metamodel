package nabla2.generator.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.OrdinalNumber
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.Entity
/**
 * 自動生成テンプレート
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class GeneratorTemplate {

  /** 論理名 */
  static final String ENTITY_NAME = "自動生成テンプレート"

  // ----- プロパティ定義 ------ //

  /**
   * タスク名
   */
  SingleLineText taskName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 生成元エンティティ名
   */
  SingleLineText generateFromEntityName
  /**
   * 名前空間
   */
  JavaNamespace namespace
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 最大生成数
   */
  OrdinalNumber generationMaxCount
  /**
   * 出力先パス
   */
  SingleLineText targetPath
  /**
   * 生成対象ファイルパターン
   */
  SingleLineText targetFilePattern
  /**
   * 再生成対象マーカ
   */
  SingleLineText regenerationMark
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 生成元エンティティ
   */
  @Memoized
  @JsonIgnore
  Entity getGenerateFrom() {
    Entity.from(_table$).filter {
      this.generateFromEntityName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<GeneratorTemplate> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new GeneratorTemplate(
            taskName : new SingleLineText(row['タスク名']),
            name : new SingleLineText(row['名称']),
            generateFromEntityName : new SingleLineText(row['生成元エンティティ名']),
            namespace : new JavaNamespace(row['名前空間']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            generationMaxCount : new OrdinalNumber(row['最大生成数']),
            targetPath : new SingleLineText(row['出力先パス'] ?: "./"),
            targetFilePattern : new SingleLineText(row['生成対象ファイルパターン'] ?: "*"),
            regenerationMark : new SingleLineText(row['再生成対象マーカ'] ?: "@author nabla2.metamodel.generator"),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  /**
   * 完全修飾名
   */
  @Memoized
  String getFqn() {
    namespace + (namespace.empty ? '' : '.') + className
  }

  /**
   * クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.orElse('')
  }

  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}