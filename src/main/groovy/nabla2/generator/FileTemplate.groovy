package nabla2.generator

import nabla2.metamodel.model.GeneratorOption

import java.util.zip.ZipEntry
import java.util.zip.ZipFile

trait FileTemplate {

  abstract boolean needsRegenerated(File file)

  abstract String getRelPath()

  abstract InputStream getResourceAsStream()

  private Map<String, GeneratorOption> generatorOptions

  Map<String, GeneratorOption> getGeneratorOptions() {
    return generatorOptions
  }

  void setGeneratorOptions(Map<String, GeneratorOption> options) {
    this.generatorOptions = options
  }

  void generateTo(File destDir) {
    File dest = new File(destDir, relPath)
    println "Writing a file to : ${dest.toPath().normalize()}"
    dest.parentFile.mkdirs()
    if (!needsRegenerated(dest)) return
    if (!dest.exists()) {
      dest.createNewFile()
    }
    getResourceAsStream().withStream { istream ->
      dest.withOutputStream { ostream ->
        ostream << istream
      }
    }
    if (dest.name.endsWith('.zip')) {
      unzip(dest)
    }
  }

  void unzip(File archive) {
    new ZipFile(archive).withCloseable { zip ->
      zip
      .entries()
      .findAll { !it.directory }
      .each { ZipEntry entry ->
        File file = new File(archive.parent, entry.name)
        file.parentFile.mkdirs()
        if (!file.exists()) file.createNewFile()
        file.withOutputStream {
           it << zip.getInputStream(entry)
        }
        file.setExecutable(true)
      }
    }
  }
}